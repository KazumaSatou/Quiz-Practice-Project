<%-- 
    Document   : StudentTakeQuiz
    Created on : Jun 21, 2023, 7:41:46 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Taking Quiz Page</title>
        <script>
            // Thiết lập thời gian bắt đầu và thời gian kết thúc
            // Thiết lập thời gian bắt đầu và thời gian kết thúc
            var startTime = new Date().getTime();

           
            var timeString = '<%= request.getAttribute("time") %>';

            // Chuyển đổi chuỗi thời gian thành các giá trị giờ, phút, giây
            var hour = parseInt(timeString.substring(0, 2));
            var minute = parseInt(timeString.substring(3, 5));
            var second = parseInt(timeString.substring(6, 8));

            // Tính toán thời gian kết thúc dựa trên giờ, phút và giây
            var endTime = startTime + (hour * 60 * 60 * 1000) + (minute * 60 * 1000) + (second * 1000);

            // Cập nhật đồng hồ đếm ngược
            function updateCountdown() {
                var currentTime = new Date().getTime();
                var remainingTime = endTime - currentTime;

                // Tính toán phút và giây còn lại
                var minutes = Math.floor((remainingTime % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((remainingTime % (1000 * 60)) / 1000);

                // Hiển thị đồng hồ đếm ngược
                var countdownElement = document.getElementById("countdown");
                countdownElement.innerHTML = minutes + " phút " + seconds + " giây ";

                // Kiểm tra nếu thời gian đã hết
                if (remainingTime <= 0) {
                    // Tự động nộp bài hoặc thực hiện hành động khi thời gian kết thúc
                    // Thay đổi action của form tại đây nếu muốn tự động nộp bài
                    document.querySelector('.btn.btn-danger').click();
                }
            }
            // Cập nhật đồng hồ đếm ngược mỗi giây
            var countdownInterval = setInterval(updateCountdown, 1000);
            // Hàm kết thúc quiz và đồng hồ đếm ngược
            function endQuiz() {
                clearInterval(countdownInterval); // Dừng đồng hồ đếm ngược
              
            }
            document.querySelector('form').addEventListener('submit', function () {
                endQuiz(); // Stop the countdown timer when the form is submitted
            });
        </script>
    </head>
    <body>
        <style>
            section{
                padding-left: 2%
            }
            #countdown {
                font-size: 24px;
                font-weight: bold;
                text-align: center;
            }
        </style>
        <%@include file="HomePageStudent.jsp" %>
        <main role="main">
            <section class="panel important">
                <div id="countdown"></div>

                <form method="POST" action="StudentTakeQuizController">
                    <c:set var="listSize" value="0" />
                    <c:forEach var="x" items="${listQuestion}" varStatus="loop">
                        <c:set var="listSize" value="${listSize + 1}" />

                        <input style="display: none;" type="text" name="lengthOfQuiz" value="${x.questionID}"/>

                        <p><span>${loop.count}</span>: <span>${x.content}</span></p>
                        <c:forEach var="dto" items="${listAllAnswers}" varStatus="count">
                            
                            <ol type="A">
                                <c:if test="${dto.questionID eq x.questionID}">
                                    <li><input type="checkbox" name="Answer${dto.questionID}" value="${dto.answerText}"/> ${dto.answerText}</li>
                                    </c:if>
                            </ol>                         
                        </c:forEach>
                        <br>
                    </c:forEach>
                    <input type="hidden" name="quizID" value="${quizID}" />
                    <input style="margin: 30px 0; " type="submit" class="btn btn-danger" value="Submit"/>
                    
                </form>
            </section>
        </main>
    </body>
</html>

