<%-- 
    Document   : Chapter_Detail
    Created on : Jun 16, 2023, 7:26:58 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="entity.Chapter"%>
<%@page import="entity.BankingQuestion"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chapter</title>
    </head>
    <body>
        <h1>Chapter Detail</h1>
        <h2>Chapter Name: ${chapter.chapterName}</h2>
        <h3>Chapter ID: ${chapter.chapterID}</h3>
        <ul>
            <table>
                <thead>
                    <tr>
                        <th>Banking Question ID</th>
                        <th>Question Content</th>
                    </tr>
                </thead>
<!--                <tbody>
                    <c:forEach items="${bankingQuestionList}" var="bankingQuestion">
                        <tr>
                            <td>${bankingQuestion.bankingID}</td>
                            <td>${bankingQuestion.questionContent}</td>
                        </tr>
                    </c:forEach>
                </tbody>-->
            </table>
        </ul>
    </body>

    <a href="TeacherController">Back to Subject Detail</a>
</body>
</html>
