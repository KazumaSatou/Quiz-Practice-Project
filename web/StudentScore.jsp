<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Quiz Result</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                height: 100vh;
                margin: 0;
            }

            .container {
                margin-top: 300px;
                border: 1px solid #ccc;
                padding: 20px;
                border-radius: 5px;
                text-align: center;
                background-color: #f9f9f9;
            }

            h1 {
                color: #333;
            }

            p {
                margin-bottom: 10px;
            }

            .btn-back {
                padding: 10px 20px;
                background-color: #4CAF50;
                color: #fff;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            .btn-back:hover {
                background-color: #45a049;
            }

            .true-value {
                color: #2ecc71;
                font-weight: bold;
            }

            .false-value {
                color: #e74c3c;
                font-weight: bold;
            }
        </style>
        <script>
            function goBackTwo() {
                history.go(-2);
            }
        </script>
    </head>
    <body>
        <div class="container">
            <h1>Quiz Result</h1>
            <%-- Retrieve the quiz result and true answer count from the controller --%>
            <% int trueAnswerCount = (int) request.getAttribute("trueAnswerCount"); %>
            <% float quizResult = (float) request.getAttribute("quizResult"); %>
            <p>True Answer is: <%= trueAnswerCount %> / <%= request.getParameterValues("lengthOfQuiz").length %> answers</p>
            <p>You scored: <%= quizResult %> points</p>
            <button onclick="goBackTwo()" class="btn-back">Back</button>
            <br><br>
            <%-- Loop through the list of questions and their corresponding answers --%>
            <c:forEach var="question" items="${listQuestion}" varStatus="loop">
                <p><span>${loop.count}</span>: <span>${question.content}</span></p>
                <%-- Loop through the list of answers using a nested forEach --%>
                    <c:forEach var="answer" items="${listAllAnswers}" varStatus="count">
                        <%-- Check if the answer corresponds to the current question --%>
                        <c:if test="${answer.questionID eq question.questionID}">
                            <li>
                                <input type="" name="Answer${answer.questionID}" value="${answer.answerText}"/>
                                <c:choose>
                                    <c:when test="${answer.is_Correct}">
                                        <span class="true-value">&#x2713;</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="false-value">&#x25cf;</span>
                                    </c:otherwise>
                                </c:choose>
                            </li>
                        </c:if>
                    </c:forEach>
                <br>
            </c:forEach>
        </div>
    </body>
</html>
