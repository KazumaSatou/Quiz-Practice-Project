<%-- 
    Document   : adminSubject
    Created on : Jun 7, 2023, 12:37:13 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Admin Page</title>
        <link
            rel="stylesheet"
            href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
            />
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous"
            />
        <script src="https://kit.fontawesome.com/64bb7a6643.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/admin.css" />  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    </head>
    <body>
        <div class="sidebar">
            <div class="sidebar-brand">
                <h2><span class="las la-accusoft"></span>Admin Page</h2>
            </div>

            <div class="sidebar-menu">
                <ul>
                    <li>
                        <a href="adminHomePage"
                           ><i class="fas fa-user-circle"></i><span>All</span></a
                        >
                    </li>
                    <li>
                        <a href="createAccount"
                           ><i class="fas fa-book-open"></i><span>Create Account</span></a
                        >
                    </li>
                    <li>
                        <a href="ViewSubjectController"  class="active"
                           ><i class="fas fa-laptop"></i>
                            <span>Subject</span></a
                        >
                    </li>
                    <li>
                        <a href="CreateSubjectController"
                           ><i class="fas fa-lock-open"></i><span>Create Subject</span></a
                        >
                    </li>
                    <li>
                        <a href="ViewClassController"  
                           ><i class="fas fa-user"></i>
                            <span>Class</span></a
                        >
                    </li>
                    <li>
                        <a href="CreateClassController"
                           ><i class="fas fa-lock-open"></i><span>Create Class</span></a
                        >
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-content">
            <header>
                <h2>
                    <label for="">
                        <span class="las la-bars"></span>
                    </label>
                    Dashboard
                </h2>
                <div class="user-wrapper">
                    <div>
                        <h4>Admin</h4>
                    </div>
                </div>
            </header>

            <main>

                <div class="container-fluid">
                    <h2 class="mt-3">Subject Management</h2>
                    <table class="table table-striped table-bordered mt-4">
                        <thead>
                            <tr>
                                <th scope="col">Subject ID</th>
                                <th scope="col">Subject Name</th>
                                <th scope="col">Subject Code</th>
                                <th scope="col" colspan="1" style="text-align: center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.list}" var="i">
                                <tr>
                                    <td>${i.subjectID}</td>
                                    <td>${i.subjectName}</td>
                                    <td>${i.subjectCode}</td>
                                    <td>
                                        <button
                                            type="button"
                                            class="btn btn-primary"
                                            data-toggle="modal"
                                            data-target="#exampleModal"
                                            onclick="updateData(${i.subjectID})"
                                            >
                                            Edit
                                        </button>
                                    </td>

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </main>
        </div>
        <div
            class="modal fade"
            id="exampleModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
            >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close"
                            >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body container-fluid">
                        <form action="editSubjectController" method="post" class="row" id="update">

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $("#myModal").on("shown.bs.modal", function () {
                $("#myInput").trigger("focus");
            });
            function updateData(i) {
                $.ajax({
                    type: 'POST',
                    url: '${pagecontext.request.contextpath}/QuizPractice/ViewSubjectController',
                    data: {
                        subjectID: i
                    },
                    success: function (data, textStatus, jqXHR) {
                        $('#update').html(data);
                    }
                })
            }
        </script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"
        ></script>


    </body>


</html>
