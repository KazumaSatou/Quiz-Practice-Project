<%-- 
    Document   : CreateQuiz
    Created on : Jun 7, 2023, 9:56:32 AM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Create Quiz</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 20px;
                background-color: #f2f2f2;
            }

            .container {
                max-width: 600px;
                margin: 0 auto;
                background-color: #fff;
                padding: 20px;
                border-radius: 5px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            }

            h1 {
                color: #333;
                text-align: center;
                margin-bottom: 30px;
            }

            label {
                display: block;
                margin-top: 10px;
                font-weight: bold;
            }

            input[type="text"],
            input[type="number"],
            input[type="date"] {
                width: 100%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 3px;
                box-sizing: border-box;
            }

            input[type="submit"] {
                padding: 10px 20px;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                margin-top: 20px;
            }

            input[type="submit"]:hover {
                background-color: #0056b3;
            }

            .error-message {
                color: red;
                margin-top: 10px;
                font-size: 14px;
            }
            .btn-back  {
                display: inline-block;
                padding: 8px 35px;
                background-color:  #007bff;
                color: #fff;
                text-decoration: none;
                border-radius: 5px;
                transition: background-color 0.3s ease;
                margin-left: 100px;
                margin-top: 10px;
                margin-bottom: 20px;
                font-size: 16px;
                border: none;
                cursor: pointer;
            }

            .btn-back:hover {
                background-color: #0056b3;
            }
        </style>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
    </head>
    <body>
        <div class="container">
            <h1>Create Quiz</h1>
            <form method="POST" action="CreateQuizController">
                <input type="hidden" name="subjectID" id="subjectID" value="<%= request.getParameter("subjectID")%>"required>
                <label for="quizTitle">Quiz Title:</label>
                <input type="text" name="quizTitle" id="quizTitle" required>

                <label for="description">Description:</label>
                <input type="text" name="description" id="description" required>

                <label for="numberQuestion">Number of Questions:</label>
                <input type="number" name="numberQuestion" id="numberQuestion" required>

                <label for="startDate">Start Date:</label>
                <input type="date" name="startDate" id="startDate" required>

                <label for="endDate">End Date:</label>
                <input type="date" name="endDate" id="endDate" required>

                <label for="times">Time:</label>
                <input type="text" name="times" id="times" pattern="([0-9]{2}):([0-9]{2}):([0-9]{2})" required>



                <input type="submit" value="Create Quiz">
                <button onclick="goBack()" class="btn-back">Back</button>
                <%-- Add error handling if needed --%>
                <%-- <p class="error-message">${errorMessage}</p> --%>
            </form>
        </div>  

    </body>
</html>