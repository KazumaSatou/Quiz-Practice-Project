<%-- 
    Document   : ChangePasswordSuccess
    Created on : Jun 6, 2023, 7:52:42 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change Password Success</title>
        <style>
            body {
                background-color: #f1f1f1;
                font-family: Arial, sans-serif;
            }

            .container {
                max-width: 500px;
                margin: 0 auto;
                padding: 20px;
                background-color: #fff;
                border-radius: 5px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            }

            h1 {
                text-align: center;
                color: #333;
            }

            p {
                text-align: center;
                color: #666;
            }

            .login-link {
                display: block;
                text-align: center;
                margin-top: 20px;
            }

            .login-link a {
                color: #666;
                text-decoration: none;
                font-size: 25px;
                font-weight: bold;
                color: blue;
            }

            .login-link a:hover {
                color: #333;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Change Password Success</h1>
            <p>Your password has been changed successfully.</p>
            <div class="login-link">
                <a href="Login.jsp">Return to Login</a>
            </div>
        </div>
    </body>
</html>
