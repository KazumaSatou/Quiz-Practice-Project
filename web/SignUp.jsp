<%-- 
    Document   : SignUp
    Created on : May 24, 2023, 10:41:31 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String error1 = (String)request.getAttribute("error1"); %>
<%String error2 = (String)request.getAttribute("error2"); %>
<%String error3 = (String)request.getAttribute("error3"); %>
<%String error4 = (String)request.getAttribute("error4"); %>
<%String error5 = (String)request.getAttribute("error5"); %>
<%String error6 = (String)request.getAttribute("error6"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body {
                font-family: Arial, sans-serif;
            }

            h2 {
                color: #333;
            }

            form {
                margin-top: 20px;
            }

            label {
                display: block;
                margin-bottom: 5px;
                font-weight: bold;
            }

            input[type="text"],
            input[type="password"],
            input[type="email"] {
                width: 200px;
                padding: 5px;
                margin-bottom: 10px;
            }

            input[type="submit"] {
                padding: 10px 20px;
                background-color: #333;
                color: #fff;
                border: none;
                cursor: pointer;
            }

            input[type="submit"]:hover {
                background-color: #555;
            }
        </style>
    </head>
    <body>
        <h2>Sign Up</h2>
        <form action="SignUpController">
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" oninvalid="Please fill in this field" required ><br>
            <% if(error1!=null){%>

            <p><%= error1%></p>
            <%
                }
            %>
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" oninvalid="Please fill in this field" required ><br>
            <% if(error2!=null){%>

            <p><%= error2%></p>
            <%
                }
            %>
            <label for="confirmPassword">ConfirmPassword:</label>
            <input type="password" id="confirmPassword" name="confirmPassword" oninvalid="Please fill in this field" required ><br>
            <% if(error3!=null){%>

            <p><%= error3%></p>
            <%
                }
            %>
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" oninvalid="Please fill in this field" required ><br>
            <% if(error4!=null){%>

            <p><%= error4%></p>
            <%
                }
            %>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>
