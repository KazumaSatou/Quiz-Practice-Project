<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="entity.Quiz" %>
<%@ page import="entity.Question" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Test</title>
        <style>
            body {
                font-family: Arial, sans-serif;
            }

            h1 {
                color: #333;
                text-align: center;
            }

            h2 {
                color: #666;
            }

            .container {
                display: flex;
            }

            .question-list {
                flex: 1;
                padding-right: 20px;
            }

            .question-list h3 {
                color: #333;
            }

            .question-list table {
                width: 100%;
                border-collapse: collapse;
            }

            .question-list th,
            .question-list td {
                padding: 8px;
                text-align: left;
                border: 1px solid #ccc;
            }

            .add-question {
                flex: 1;
                padding-left: 20px;
            }

            .add-question h3 {
                color: #333;
            }

            .add-question form {
                display: flex;
                flex-direction: column;
                align-items: flex-start;
            }

            .add-question label {
                margin-bottom: 5px;
                font-weight: bold;
                color: #666;
            }

            .add-question input[type="text"] {
                padding: 5px;
                width: 200px;
                margin-bottom: 10px;
            }

            .add-question input[type="submit"] {
                padding: 5px 10px;
                background-color: #333;
                color: #fff;
                border: none;
                cursor: pointer;
            }

            .add-question input[type="submit"]:hover {
                background-color: #666;
            }
            .save-button {
                display: inline-block;
                padding: 8px 16px;
                background-color: #428bca;
                color: #fff;
                text-decoration: none;
                border-radius: 4px;
                transition: background-color 0.3s ease;
            }

            .save-button:hover {
                background-color: #3071a9;
            }
            .question-list1 {
                flex: 1;
                padding-right: 20px;
            }

            .question-list1 h3 {
                color: #333;
            }

            .question-list1 table {
                width: 100%;
                border-collapse: collapse;
            }

            .question-list1 th,
            .question-list1 td {
                padding: 8px;
                text-align: left;
                border: 1px solid #ccc;
            }
        </style>
        <script>
            function filterQuestionsByChapter() {
                const selectedChapter = document.getElementById("chapterFilter").value;
                const questionRows = document.querySelectorAll(".question-list tbody tr");

                for (let i = 0; i < questionRows.length; i++) {
                    const chapterCell = questionRows[i].querySelector("td:nth-child(3)");

                    if (selectedChapter === "" || chapterCell.textContent === selectedChapter) {
                        questionRows[i].style.display = "";
                    } else {
                        questionRows[i].style.display = "none";
                    }
                }
            }
        </script>
    </head>
    <body>
        <h1 style="font-size: 45px;color:green">Test</h1>
        <h2>Quiz: ${quiz.quizTitle}</h2>
        <div class="add-question" style="margin-left: 15px">
            <h3>Add Question to Test</h3>
            <form method="POST" action="AddQuestionToTestController" style="margin-left: 20px">
                <input type="hidden" name="quizId" value="${quiz.quizId}">
                <input type="hidden" name="subjectId" value="${quiz.subjectId}">
                <label for="questionId">Question ID:</label>
                <input style="height: 30px;" type="number" name="questionId" id="questionId" required><br>
                <input type="submit" value="Add Question to Test">
            </form>
        </div>



        <div> 
            <br>
            <a href="ManagerQuizController?subjectID=${quiz.subjectId}" class="save-button" style="margin-left: 50px">Save</a>
        </div>

        <hr>

        <div class="container">
            <div class="question-list">
                <label for="chapterFilter">Filter by Chapter:</label>
                <select style="height: 40px;" id="chapterFilter" onchange="filterQuestionsByChapter()">
                    <option value="">All Chapters</option>
                    <!-- Loop through available chapters and create options -->
                    <c:forEach items="${requestScope.lmao}" var="chapter">
                        <option value="${chapter.chapterID}">${chapter.chapterID}</option>
                    </c:forEach>
                </select>
                <h2 style="color: #666">All Questions</h2>
                <table>
                    <thead style="background-color: #343a40;color: #fff">
                        <tr>
                            <th scope="col">Question ID</th>
                            <th scope="col">Content</th>
                            <th scope="col">Chapter ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.list}" var="i">
                            <tr>
                                <td>${i.questionID}</td>
                                <td>${i.content}</td>
                                <td>${i.chapterID}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

            

            <div class="question-list1">
                <br><br>
                <h2 style="color: #666">Question in quiz</h2>
                <table>
                    <thead style="background-color: #343a40;color: #fff">
                        <tr>
                            <th scope="col">Question ID</th>
                            <th scope="col">Content</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.load}" var="o">
                            <tr>
                                <td>${o.questionID}</td>
                                <td>${o.content}</td>
                                <td>                        
                                    <form action="RemoveQuesController" method="post">
                                        <input type="hidden" name="quizID" value="${quiz.quizId}">
                                        <input type="hidden" name="subjectID" value="${quiz.subjectId}">
                                        <input type="hidden" name="questionID" value="${o.questionID}">
                                        <input type="submit" value="Remove">
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
    <script>

        if (sessionStorage.getItem("testId")) {
            // Retrieve the stored value and set it as the default value for the testId input
            document.getElementById("testId").value = sessionStorage.getItem("testId");
            // Set the testId input as readonly
            document.getElementById("testId").readOnly = true;
        }


        document.querySelector("form").addEventListener("submit", function (e) {
           
            if (!sessionStorage.getItem("testId")) {
                // Store the value of testId in sessionStorage if it doesn't exist
                sessionStorage.setItem("testId", document.getElementById("testId").value);
            }
        });
    </script>
</html>