<%-- 
    Document   : header
    Created on : May 29, 2023, 11:12:38 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.Users"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% 
       Users currUser = (Users)request.getSession().getAttribute("currUser");
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Account Information</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <div>
            <ul>
                <% if(currUser==null){ %>
                <li >
                    <a  href="Login.jsp">Login</a>
                </li>
                <%}else{%>
                <!--
                                <span > Welcome: <%=currUser.getAccount()%></span>-->

                <h1>Welcome ${currUser.account}!</h1>
                <p>Account: ${currUser.account}</p>
                <p>Email: ${currUser.email}</p>
                <p>Phone: 0${currUser.phone}</p>
                <p>Address: ${currUser.address}</p>


                <!--            <%  if(currUser.getRoleID() == 1){ %>
                           <li >
                              <a  href="admin.jsp">Infor</a> 
                           </li>
                <%}%>-->
                <!--            <%  if(currUser.getRoleID() == 2){ %>
                          <li >
                               <a  href="HomePageTeacher.jsp">Personal Account</a>
                           </li>
                <%}%>-->
                <!--            <%  if(currUser.getRoleID() == 3){ %>
                          <li>
                              <a  href="HomePageStudent.jsp">Personal Account</a>
                          </li>
                <%}%> -->
                <li >
                    <a  href="ChangePassword.jsp">ChangePassword</a>
                </li>
                <li >
                    <a  href="LogoutController">Logout</a>
                </li>
                <%}%>
            </ul>
            <!--        <form class="form-inline my-2 my-lg-0" action="SearchByDescription" >
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="animalName">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                  <form>-->
        </div>
    </body>
</html>


