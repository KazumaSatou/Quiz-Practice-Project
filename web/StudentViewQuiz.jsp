<%-- 
    Document   : StudentViewQuiz
    Created on : Jun 21, 2023, 7:42:22 AM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quiz Page</title>

    </head>
    <body>
        <%@include file="HomePageStudent.jsp" %>
        <style>
            .center-container {
                display: flex;
                justify-content: center;
                align-items: center;
            }
        </style>
        <main role="main">
            <section class="panel important">
                <table class="table" id="table">
                    <thead>
                        <tr>
                            <th scope="col">Quiz ID</th>
                            <th scope="col">QUIZ TITLE</th>
                            <th scope="col">TIME</th>
                            <th scope="col">OPERATOR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            
                            List<Quiz> list = (List<Quiz>) request.getAttribute("listQuiz");

                            
                            if (list != null) {
                                for (Quiz quiz : list) {
                                    
                        %> <tr>
                            <td><% out.println(quiz.getQuizId()); %></td>
                            <td><% out.println(quiz.getQuizTitle()); %></td>
                            <td><% out.println(quiz.getTimes().substring(0, 8)); %></td>
                            <td>
                                <button type="button" class="btn btn-primary btnViewQuiz">
                                    <a style="text-decoration: none; color: white;" href="StudentTakeQuizController?QuizID=<% out.println(quiz.getQuizId()); %>">Take Quiz</a>
                                </button>
                        </tr>
                        <%
                                }
                            }
                        %>
                    <div class="center-container">
                        <button class="btn btn-danger viewScoreButton">
                            <a href="StudentScoreController" style="text-decoration: none; color: white;">View score</a>
                        </button>
                    </div>
                    </tbody>

                </table>
            </section>
        </main>
    </body>
</html>

