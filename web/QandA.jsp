<%-- 
    Document   : QandA
    Created on : Jun 18, 2023, 1:18:14 AM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create Question</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f1f1f1;
                padding: 20px;
            }

            .container {
                max-width: 400px;
                margin: 0 auto;
                background-color: #ffffff;
                border-radius: 4px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                padding: 20px;
            }

            h1 {
                text-align: center;
                margin-top: 0;
            }

            form {
                margin-top: 20px;
            }

            label {
                font-weight: bold;
                display: block;
                margin-bottom: 10px;
            }

            input[type="text"], select {
                width: 100%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                margin-bottom: 20px;
            }

            input[type="submit"] {
                background-color: #4CAF50;
                color: white;
                padding: 10px 20px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type="submit"]:hover {
                background-color: #45a049;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Create Question</h1>
            <form action="#" method="POST">
                <label for="question">Question:</label>
                <input type="text" id="question" name="question" required>

                <label for="option1">A</label>
                <input type="text" id="option1" name="option1" required>

                <label for="option2">B</label>
                <input type="text" id="option2" name="option2" required>

                <label for="option3">C</label>
                <input type="text" id="option3" name="option3" required>

                <label for="option4">D</label>
                <input type="text" id="option4" name="option4" required>

                <label for="answerType">Type:</label>
                <select id="answerType" name="answerType">
                    <option value="single">Only choice</option>
                    <option value="multiple">Multi-choice</option>
                </select>

                <label for="correctAnswer">Answer:</label>
                <input type="text" id="correctAnswer" name="correctAnswer" required>

                <input type="submit" value="Add">
            </form>
        </div>
    </body>
</html>

