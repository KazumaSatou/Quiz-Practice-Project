<%-- 
    Document   : createTeacher
    Created on : May 30, 2023, 10:32:28 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link
            rel="stylesheet"
            href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
            />
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous"
            />
        <script src="https://kit.fontawesome.com/64bb7a6643.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/admin_course.css">
        <title>Document</title>
    </head>
    <body>
        <div class="sidebar">
            <div class="sidebar-brand">
                <h2><span class="las la-accusoft"></span>Admin Page</h2>
            </div>

            <div class="sidebar-menu">
                <ul>
                    <li>
                        <a href="adminHomePage" 
                           ><i class="fas fa-user-circle"></i><span>All</span></a
                        >
                    </li>
                    <li>
                        <a href="createAccount" class="active"
                           ><i class="fas fa-book-open"></i><span>Create Account</span></a
                        >
                    </li>
                    <li>
                        <a href="ViewSubjectController" 
                           ><i class="fas fa-laptop"></i>
                            <span>Subject</span></a
                        >
                    </li>
                    <li>
                        <a href="CreateSubjectController"
                           ><i class="fas fa-lock-open"></i><span>Create Subject</span></a
                        >
                    </li>
                    <li>
                        <a href="ViewClassController"  
                           ><i class="fas fa-user"></i>
                            <span>Class</span></a
                        >
                    </li>
                    <li>
                        <a href="CreateClassController"
                           ><i class="fas fa-lock-open"></i><span>Create Class</span></a
                        >
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-content">
            <header>
                <h2>
                    <label for="">
                        <span class="las la-bars"></span>
                    </label>
                    Dashboard
                </h2>
                <div class="user-wrapper">
                    <div>
                        <h4>Admin</h4>
                    </div>
                </div>
            </header>

            <main>
                <div class="container-fluid">
                    <div class="row justify-content-between subject-section">
                        <h2 class="mt-3">Create Account</h2>
                    </div>

                    <form action="createAccount" method="post" class="row">
                        <div class="col-6">
                            <label for="exampleInputEmail1">Account</label>
                            <p class="text-danger">${error1}</p>
                            <input
                                name="account"
                                type="text"
                                class="form-control"
                                id="inputAccount"
                                aria-describedby="emailHelp"
                                placeholder="Enter account"
                                value="${enteredAccountValue}"
                                />  
                            <div id="errorAccount"></div>
                        </div>
                        <div id="error"></div>
                        <div class="col-6">
                            <label for="exampleInputEmail1">Email</label>
                            <p class="text-danger">${error3}</p>
                            <input
                                name="email"
                                type="email"
                                class="form-control"
                                id="inputEmail"
                                aria-describedby="emailHelp"
                                placeholder="Enter email"
                                onchange="checkEmailExisted()"
                                value="${enteredEmailValue}"
                                />
                        </div>
                        <div id="error"></div>
                        <div class="col-6">
                            <label for="exampleInputEmail1">Phone</label>
                            <p class="text-danger">${error6}</p>
                            <input
                                name="phone"
                                type="text"
                                class="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Enter phone"
                                value="${enteredPhoneValue}"
                                />               
                        </div>
                        <div class="col-6">
                            <label for="exampleInputEmail1">Address</label>
                            <p class="text-danger">${error7}</p>
                            <input
                                name="address"
                                type="text"
                                class="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Enter address"
                                value="${enteredAddressValue}"
                                />               
                        </div>
                        <div class="col-6">
                            <label for="exampleInputPassword1">Password</label>
                            <p class="text-danger">${error2}</p>
                            <input
                                name="password"
                                type="password"
                                class="form-control"
                                id="exampleInputPassword1"
                                placeholder="Enter password"
                                value="${enteredPasswordValue}"
                                />
                        </div>
                        <div class="col-6">
                            <label for="exampleInputEmail1">Role</label>
                            <select name="role" class="form-control">
                                <option value="2">Student</option>
                                <option value="3">Teacher</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <label for="exampleInputEmail1">FullName</label>
                            <p class="text-danger">${error8}</p>
                            <input
                                name="fullname"
                                type="text"
                                class="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Enter FullName"
                                value="${enteredFullnameValue}"
                                />               
                        </div>
                        <div class="col-6">
                            <label for="exampleInputEmail1">Age</label>
                            <p class="text-danger">${error9}</p>
                            <input
                                name="age"
                                type="number"
                                class="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Enter Age"
                                value="${enteredAgeValue}"
                                />               
                        </div>
                        <div class="col-6">
                            <label for="exampleInputEmail1">Gender</label>
                            <select name="gender" class="form-control">
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>           
                        </div>
                        <div class="col-12" style="margin-left: 44%;margin-top: 4%;">
                            <button type="submit" id="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </main>

        </div>       
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script type="text/javascript">
                                    function checkEmailExisted() {
                                        $.ajax({
                                            type: 'POST',
                                            url: '${pagecontext.request.contextpath}/QuizPractice/checkEmailExisted',
                                            data: {
                                                email: $('#inputEmail').val()
                                            },
                                            success: function (data, textStatus, jqXHR) {
                                                if (data) {
                                                    $('#error').html(data);
                                                    $('#submit').prop("disabled", true);
                                                } else {
                                                    $('#error').html('');
                                                    $('#submit').prop("disabled", false);
                                                }
                                            }
                                        })
                                    }

                                    function checkAccountExisted() {
                                        $.ajax({
                                            type: 'GET',
                                            url: '${pagecontext.request.contextpath}/QuizPractice/checkEmailExisted',
                                            data: {
                                                account: $('#inputAccount').val()
                                            },
                                            success: function (data, textStatus, jqXHR) {
                                                if (data) {
                                                    $('#errorAccount').html(data);
                                                    $('#submit').prop("disabled", true);
                                                } else {
                                                    $('#errorAccount').html('');
                                                    $('#submit').prop("disabled", false);
                                                }
                                            }
                                        })
                                    }
    </script>
</html>
