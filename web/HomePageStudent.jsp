<%-- 
   Document   : HomePageStudent
   Created on : Jun 2, 2023, 11:30:48 PM
   Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dao.*"%>
<%@page import="entity.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.ResultSet"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Page</title>
        <link rel="stylesheet" href="./css/HomePageStudent.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

    </head>
    <body>
        <header role="banner">
            <h1>Welcome ${student.get(1)}!</h1>
            <ul class="utilities">
                <br>


                <li class="users"><a href="header.jsp">My Account</a></li>
                <li class="logout warn"><a href="LogoutController">Log Out</a></li>
            </ul>

        </header>

        <nav role='navigation'>
            <ul class="main" style="padding: 0;">
                <!--                <li class="dashboard"><a href="StudentController">View Subject</a></li>-->
                <li class="edit"><a href="StudentViewEnrollSubject?StudentID=${student.get(0)}">My Class</a></li>
                <!--<li class="write"><a href="CreateQuiz.jsp">Create Quiz</a></li>-->
            </ul>
        </nav>

    </body>
</html>
