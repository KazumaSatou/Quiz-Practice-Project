<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Score Page</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }

            th, td {
                border: 1px solid #ccc;
                padding: 8px;
                text-align: left;
            }

            th {
                background-color: #f2f2f2;
            }

            h2 {
                margin-top: 0;
            }

            .panel {
                background-color: #fff;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                padding: 20px;
            }

            .important {
                background-color: #ffeeba;
            }
        </style>
    </head>
    <body>
        <%@include file="HomePageStudent.jsp" %>
        <main role="main">
            <section class="panel important">
                <h2>Student Scores</h2>

                <table>
                    <thead>
                        <tr>
                            <th>Quiz ID</th>
                            <th>Quiz Title</th>
                            <th>Subject</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="i" items="${requestScope.quizzes}" varStatus="loop">
                            <tr>
                                <td>${i.quizId}</td>
                                <td>${i.quizTitle}</td>
                                <td>${requestScope.subjects[loop.index].subjectCode}</td>
                                <td>${requestScope.scores[loop.index].totalScore}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </section>
        </main>
    </body>
</html>
