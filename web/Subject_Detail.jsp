<%-- 
    Document   : Subject_Detail
    Created on : Jun 14, 2023, 8:14:36 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="entity.Subject"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Subject Detail</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f5f5f5;
                margin: 0;
                padding: 0;
            }

            .container {
                max-width: 800px;
                margin: 0 auto;
                padding: 20px;
            }

            h1 {
                color: #333;
                text-align: center;
                margin-top: 40px;
            }

            h2 {
                color: #555;
                margin-top: 20px;
                margin-left: 150px;
            }

            h3 {
                color: #888;
                margin-bottom: 20px;
                margin-left: 150px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 20px;
                background-color: #fff;
                border-radius: 5px;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            }

            th, td {
                padding: 12px 15px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

            th {
                background-color: #f9f9f9;
            }

            a {
                color: #337ab7;
                text-decoration: none;
            }

            a:hover {
                text-decoration: underline;
            }

            .create-quiz-btn {
                display: inline-block;
                padding: 8px 35px;
                background-color:  #007bff;
                color: #fff;
                text-decoration: none;
                border-radius: 5px;
                transition: background-color 0.3s ease;
                margin-left: 200px;
            }

            .create-quiz-btn:hover {
                background-color: #23527c;
            }
            .btn-back  {
                display: inline-block;
                padding: 8px 35px;
                background-color:  #007bff;
                color: #fff;
                text-decoration: none;
                border-radius: 5px;
                transition: background-color 0.3s ease;
                margin-left: 100px;
                margin-top: 10px;
                margin-bottom: 20px;
                font-size: 16px;
                border: none;
                cursor: pointer;
            }

            .btn-back:hover {
                background-color: #0056b3;
            }
        </style>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
    </head>
    <body>
        <h1>Subject Detail</h1>
        <h2>Subject Name: ${subject.subjectName}</h2>
        <h3>Subject Code: ${subject.subjectCode}</h3>
        <ul>
            <table>
                <thead>
                    <tr>
                        <th>Chapter ID</th>
                        <th>Chapter Name</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${chapterList}" var="chapter">
                        <tr>
                            <td>${chapter.chapterID}</td>
                            <td>${chapter.chapterName}</td>
                            <td>
                                <!--<a href="QuestionBankController?action=chapter&chapterID=${chapter.chapterID}">View Detail</a>-->
                                <a href="QuestionBankController?chapterID=${chapter.chapterID}">View Question Bank</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </ul>
        <a href="CreateQuizController?subjectID=${subject.subjectID}" class="create-quiz-btn">Create Quiz</a>
        <a href="ManagerQuizController?subjectID=${subject.subjectID}" class="create-quiz-btn">Manage Quiz</a>
        <button onclick="goBack()" class="btn-back">Back</button>
    </body>
</html>
