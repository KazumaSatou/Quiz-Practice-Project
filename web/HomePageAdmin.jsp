<%-- 
    Document   : HomePageAdmin
    Created on : Jun 2, 2023, 11:31:00 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin</title>
    </head>
    <body>
        <h1>Hello Admin!</h1>
        <h1>Chào mừng, ${currUser.account}!</h1>
        <p>Tên tài khoản: ${currUser.account}</p>
        <p>Email: ${currUser.email}</p>
        <p>Số điện thoại: ${currUser.phone}</p>
        <p>Địa chỉ: ${currUser.address}</p>
        <a href="LogoutController">Logout</a>

    </body>
</html>
