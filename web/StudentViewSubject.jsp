<%-- 
    Document   : StudentViewSubject
    Created on : Jun 21, 2023, 7:42:47 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Subject Page</title>
    </head

    <body>
        <%@include file="HomePageStudent.jsp" %>
        <main role="main">
            <section class="panel important">
                <table class="table" id="table">
                    <thead>
                        <tr>
                            <th scope="col">SUBJECT ID</th>
                            <th scope="col">SUBJECT NAME</th>
                            <th scope="col">SUBJECT CODE</th>
                            <th scope="col">CLASS ID</th>
                            <th scope="col">OPERATOR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${listSub11}" var="x" varStatus="loop">
                            <tr>
                                <td>${x.subjectID}</td>
                                <td>${x.subjectName}</td>
                                <td>${x.subjectCode}</td>
                                <td>${listClass[loop.index].classID}</td>
                                <td>
                                    <button onclick="hideTable()" type="button" class="btn btn-primary btnViewQuiz">
                                        <a style="text-decoration: none; color: white;" href="StudentViewEnrollSubject?SubjectID=${x.subjectID}">View Quiz</a>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </section>
        </main>
    </body>
</html>


