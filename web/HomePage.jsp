<%-- 
    Document   : HomePage
    Created on : May 27, 2023, 4:50:04 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body{
                margin: 2px;
                margin-bottom: 0;
            }
            .login-container {
                text-align: center;
                margin-top: 20px;
            }
            
            .logo-image{
                width: 117px;
                height: 60px;
                margin-left: -200px;
                margin-right: 50px;
            }

            .login-link {
                color: #337ab7;
                font-weight: bold;
                font-size: 30px;
                text-decoration: none;
                margin-left: 1000px;
                margin-right: -250px;
                margin-top: 20px;
            }

            .image-container {
                text-align: center;
                margin-top: 20px;
            }

            .image {
                width: 100%;
                height: 625px;
                border: 1px solid #ccc;
                border-radius: 5px;
            }
        </style>
    </head>
    <body>
        <div class="login-container">
            <a href="#" style="margin-right: 95px" >
                <img class="logo-image" src="images/logo.jpg" alt="logo"/>
            </a>
            <a href="Login.jsp" class="login-link">Login</a>
        </div>

        <div class="image-container">
            <img src="./images/QUIZZ.jpg"  alt="alt" class="image"/>
        </div>
    </body>
</html>
