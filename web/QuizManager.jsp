<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="entity.Quiz" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Quiz Management</title>
        <link
            rel="stylesheet"
            href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
            />
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous"
            />
        <script src="https://kit.fontawesome.com/64bb7a6643.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/admin.css" />  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
            }

            h1 {
                text-align: center;
                margin-bottom: 20px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
            }

            th, td {
                padding: 10px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

            th {
                background-color: #f2f2f2;
            }

            /* Button styles */
            .btn-back {
                display: block;
                margin: 20px auto;
                padding: 10px 20px;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                cursor: pointer;
            }

            /* Form styles */
            form {
                display: inline-block;
            }

            input[type="submit"] {
                background-color: #28a745;
                color: #fff;
                border: none;
                border-radius: 5px;
                padding: 5px 10px;
                cursor: pointer;
            }

            /* Status styles */
            .status-active {
                color: green;
            }

            .status-disable {
                color: red;
            }

            .status-unknown {
                color: gray;
            }
            .highlight {
                background-color: white;
                color: black;
                font-weight: bold;
            }
            .btn-link {
                display: inline-block;
                padding: 5px 10px;
                background-color: #28a745;
                color: #fff;
                border: none;
                border-radius: 5px;
                text-decoration: none;
                cursor: pointer;
            }

            .btn-link:hover {
                background-color: #218838;
            }
            .btn-back{
                display: inline-block;
                padding: 10px 20px;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                margin: 5px;
            }
        </style>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
    </head>
    <body>
        <h1>Quiz Manager</h1>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Status</th>
                    <th scope="col" colspan="3" style="text-align: center">Action</th>
                </tr>
            </thead>
            <tbody>
                <% 
                List<Quiz> list = (List<Quiz>) request.getAttribute("list");
                if (list != null) {
                    for (Quiz quiz : list) {
                %>
                <tr>
                    <td><%= quiz.getQuizId() %></td>
                    <td><%= quiz.getQuizTitle() %></td>
                    <td><%= quiz.getStartDate().substring(0, 10) %></td>
                    <td><%= quiz.getEndDate().substring(0, 10) %></td>
                    <td class="<% 
                        if (quiz.getStatus() == 1) {
                            out.print("highlight");
                        } else if (quiz.getStatus() == 0) {
                            out.print("highlight");
                        }
                        %>">
                        <% 
                        if (quiz.getStatus() == 1) {
                            out.print("Active");
                        } else if (quiz.getStatus() == 0) {
                            out.print("Disable");
                        } else {
                            out.print("Unknown");
                        }
                        %>
                    </td>
                    <td>
                        <form action="TeacherChangeStatusController" method="post">
                            <input type="hidden" name="quizID" value="<%= quiz.getQuizId() %>">
                            <input type="hidden" name="subjectID" value="<%= quiz.getSubjectId() %>">
                            <input type="hidden" name="status" value="<%= quiz.getStatus() %>">
                            <input type="submit" value="Change Status">
                        </form>
                    </td>

                    <td>
                        <form action="DeleteQuizController" method="post" onsubmit="return confirm('Are you sure you want to delete this quiz?');">
                            <input type="hidden" name="quizID" value="<%= quiz.getQuizId() %>">
                            <input type="hidden" name="subjectID" value="<%= quiz.getSubjectId() %>">
                            <input type="submit" value="Delete">
                        </form>
                    </td>
                    <td>
                        <a href="AddQuestionToTestController?quizId=<%= quiz.getQuizId() %>&subjectId=<%= quiz.getSubjectId() %>" class="btn-link">Modify</a>
                    </td>
                </tr>
                <% 
                    }
                }
                %>
            </tbody>
        </table>
        <hr>
        <button onclick="goBack()" class="btn-back">Back to previous page</button>
        <a href="ViewTeacherSubjectController" class="btn-back">Back to home page</a>
    </body>
</html>
