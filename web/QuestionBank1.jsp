<%-- 
    Document   : QuestionBank1
    Created on : Jun 20, 2023, 10:55:08 AM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="entity.*" %>
<%@ page import="java.util.List" %>
<% String chapterID = request.getParameter("chapterID");
    List<Question> questions = (List<Question>) request.getAttribute("questions"); %>
<%  List<Question> questions1 = (List<Question>) request.getAttribute("questions1"); %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Question Bank</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
                background-color: #f1f1f1;
            }

            .container {
                max-width: 800px;
                margin: 0 auto;
                background-color: #fff;
                padding: 20px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }

            h1 {
                margin-top: 0;
                color: #333;
                margin-left: 200px;
            }

            h2 {
                margin-bottom: 10px;
                color: #333;
            }

            ul {
                list-style-type: none;
                padding: 0;
            }

            li {
                margin-bottom: 10px;
                padding: 10px;
                background-color: #f9f9f9;
                border-radius: 5px;
            }

            a {
                text-decoration: none;
                color: #007bff;
            }

            textarea {
                width: 100%;
                height: 100px;
                padding: 5px;
                margin-bottom: 10px;
                border-radius: 5px;
            }

            button {
                padding: 5px 10px;
                background-color: #007bff;
                color: #fff;
                border: none;
                cursor: pointer;
                border-radius: 5px;
                margin-right: 100px;
            }

            .no-questions {
                color: #999;
            }
            .btn-back  {
                display: inline-block;
                padding: 7px 25px;
                background-color:  #007bff;
                color: #fff;
                text-decoration: none;
                border-radius: 5px;
                transition: background-color 0.3s ease;
                margin-left: 20px;
                margin-top: 10px;
                margin-bottom: 20px;
                font-size: 16px;
                border-radius: 5px;
                cursor: pointer;
            }

            .btn-back:hover {
                background-color: #0056b3;
            }

            .d-flex {
                display: flex;
                align-items: center;
                margin-bottom: 20px;
            }

            .add-question {
                margin-right: 450px;
                margin-left: 50px;
            }
        </style>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h1 style="margin-left: 390px;color:green">Question Bank</h1>

            <div class="d-flex">
                <h2 class="add-question">Add Question</h2>

                <a href="ViewTeacherSubjectController" class="btn-back">Back to home page</a>



            </div>

            <form action="QuestionBankController" method="post">
                <input type="hidden" name="action" value="addQuestion"><br>
                <div class="form-group mb-0">  
                    <label for="content">Content:</label>
                    <input type="text" name="content" id="content" required><br><br>
                    <c:if test="${not empty error}">
                        <div class="text-danger">${error}</div>
                    </c:if>

                </div>              
                <div class="form-group"> 
                    <input type="hidden" name="chapterID" value="<%= chapterID %>">
                    <div id="answerContainer">
                        <div id="answer1Container"><br>
                            <label for="answer1 mr-2" style="margin-right: 4px">Answer 1:</label>
                            <input type="text" name="answerContent" class="answer" id="answer1" required>

                            <label for="answer1Point"  style="margin-left:  4px" >Answer Point:</label>
                            <input type="text" name="answerPoint" class="answerPoint ml-2" id="answer1Point" required pattern="^[0-9]\d*$">%

                            <!--                            <label for="correct1">Correct:</label>
                                                        <input type="radio" name="correctAnswer" value="1" id="correct1">-->
                            <c:if test="${not empty error1 && param.action == 'addQuestion'}">
                                <c:if test="${param.answerContent != null}">
                                    <c:forEach var="answerContent" items="${paramValues.answerContent}" varStatus="status">
                                        <c:if test="${empty answerContent}">
                                            <div class="text-danger">Please fill out the answer completely</div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </c:if>

                            <!-- Hiển thị thông báo lỗi cho trường "Answer Point" -->
                            <c:if test="${not empty error1 && param.action == 'addQuestion'}">
                                <c:if test="${param.answerPoint != null}">
                                    <c:forEach var="answerPoint" items="${paramValues.answerPoint}" varStatus="status">
                                        <c:if test="${empty answerPoint}">
                                            <div class="text-danger">Please complete the information for the score</div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </c:if>

                            <!-- Nút Submit -->
                        </div>
                    </div>          
                </div>
                <br>
                <div class="d-flex">
                    <button type="button" onclick="addAnswer()" class="btn btn-primary mr-5" >Add Answer</button><br><br>
                    <button type="submit" class="btn btn-primary" >Save</button>               
                </div>
            </form>

            <div class="d-flex">
                <h1 style="margin-left: 400px;padding: 20px 0;color:green">Questions</h1>
                <hr>
                <form action="QuestionBankController" method="post" class="form-inline mb-3">
                    <input type="hidden" name="action" value="searchQuestions">
                    <input type="hidden" name="chapterID" value="<%= chapterID %>">
                    <div class="form-group mr-2">
                        <input type="text" name="keyword" placeholder="Search" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button><br><br>
                </form>



            </div>
            <c:forEach var="x" items="${questions}" varStatus="loop">
                <c:set var="listSize" value="${listSize + 1}" />
                <p><span style="font-size: 18px;font-weight: bold"> Question ${loop.count}</span>: <span>${x.content}</span></p>
                <c:forEach var="dto" items="${answers}" varStatus="count">
                    <!-- To do code here -->
                    <ul>
                        <c:if test="${dto.questionID eq x.questionID}">
                            <li>
                                <span class="answer">${dto.answerText}</span>
                                <span class="${dto.is_Correct ? 'correct' : 'incorrect'}">${dto.is_Correct ? '&#10004;' : ''}</span>
                            </li>
                        </c:if>
                    </ul>
                    <!-- End To do code here -->
                </c:forEach>
                <br>
                <form val="DeleteQuestionController" method="post" onsubmit="return confirmDelete();">
                    <input type="hidden" name="action" value="DeleteQuestion"><br>
                    <input type="hidden" name="questionID" value="${x.questionID}">
                    <input type="hidden" name="chapterID" value="${chapterID}">
                    <input type="submit" class="btn btn-danger mb-5" value="Delete">
                </form>
            </c:forEach>



        </div>
        <script>
            var answerCount = 1;

            function addAnswer() {
                answerCount++;

                var answerContainer = document.getElementById("answerContainer");

                var answerDiv = document.createElement("div");
                answerDiv.setAttribute("id", "answer" + answerCount + "Container");

                var answerLabel = document.createElement("label");
                answerLabel.setAttribute("for", "answer" + answerCount);
                answerLabel.innerText = "Answer " + answerCount + ":";

                var answerInput = document.createElement("input");
                answerInput.setAttribute("type", "text");
                answerInput.setAttribute("name", "answerContent");
                answerInput.setAttribute("class", "answer ml-2");
                answerInput.setAttribute("id", "answer" + answerCount);
                answerInput.setAttribute("required", "true");

                var answerPointLabel = document.createElement("label");
                answerPointLabel.setAttribute("for", "answer" + answerCount + "Point");
                answerPointLabel.setAttribute("class", "ml-2");
                answerPointLabel.innerText = "Answer " + "Point:";

                var answerPointInput = document.createElement("input");
                answerPointInput.setAttribute("type", "text");
                answerPointInput.setAttribute("name", "answerPoint");
                answerPointInput.setAttribute("class", "answerPoint ml-2");
                answerPointInput.setAttribute("id", "answer" + answerCount + "Point");
                answerPointInput.setAttribute("required", "true");
                answerPointInput.setAttribute("pattern", "^[0-9]\\d*$");

                // Here we use JSP to get the value of the answerPoint parameter
                var answerPointValue = '<%= request.getParameter("answerPoint") %>';
                answerPointInput.setAttribute("value", '');

                answerDiv.appendChild(answerLabel);
                answerDiv.appendChild(answerInput);
                answerDiv.appendChild(answerPointLabel);
                answerDiv.appendChild(answerPointInput);
                answerContainer.appendChild(answerDiv);
            }
            function confirmDelete() {
                return confirm("Do you really want to delete this question");
            }
        </script>


        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

    </body>
</html>