<%-- 
    Document   : adminSubject
    Created on : Jun 7, 2023, 12:37:13 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="entity.Class" %>
<%@ page import="entity.Subject" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Admin Page</title>
        <link
            rel="stylesheet"
            href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
            />
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous"
            />
        <script src="https://kit.fontawesome.com/64bb7a6643.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/admin.css" />  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    </head>
    <body>
        <div class="sidebar">
            <div class="sidebar-brand">
                <h2><span class="las la-accusoft"></span>Admin Page</h2>
            </div>

            <div class="sidebar-menu">
                <ul>
                    <li>
                        <a href="adminHomePage"
                           ><i class="fas fa-user-circle"></i><span>All</span></a
                        >
                    </li>
                    <li>
                        <a href="createAccount"
                           ><i class="fas fa-book-open"></i><span>Create Account</span></a
                        >
                    </li>
                    <li>
                        <a href="ViewSubjectController" 
                           ><i class="fas fa-laptop"></i>
                            <span>Subject</span></a
                        >
                    </li>
                    <li>
                        <a href="CreateSubjectController"
                           ><i class="fas fa-lock-open"></i><span>Create Subject</span></a
                        >
                    </li>
                    <li>
                        <a href="ViewClassController"  class="active"
                           ><i class="fas fa-user"></i>
                            <span>Class</span></a
                        >
                    </li>
                    <li>
                        <a href="CreateClassController"
                           ><i class="fas fa-lock-open"></i><span>Create a new Class</span></a
                        >
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-content">
            <header>
                <h2>
                    <label for="">
                        <span class="las la-bars"></span>
                    </label>
                    Dashboard
                </h2>
                <div class="user-wrapper">
                    <div>
                        <h4>Admin</h4>
                    </div>
                </div>
            </header>


            <main>
                <div class="container">
                    <div class="row" style="padding-bottom: 30px;">
                        <% List<Class> classList = (List<Class>) request.getAttribute("classList");%>
                        <% List<Subject> subjectList = (List<Subject>) request.getAttribute("subjectList");%>
                        <% if (classList != null && !classList.isEmpty()) { %>
                        <% for (int i = 0; i < classList.size(); i++) { %>
                        <% Class cl = classList.get(i);
                           Subject s = subjectList.get(i);
                        %>
                        <div class="col-md-4">
                            <div class="card mt-4">
                                <div class="card-body">

                                    <h6 class="card-subtitle mb-2 text-muted">Subject ID: <%= s.getSubjectID() %></h6>
                                    <h6 class="card-subtitle mb-2 text-muted">Subject Name: <%= s.getSubjectName() %></h6>
                                    <h6 class="card-subtitle mb-2 text-muted">Subject Code: <%= s.getSubjectCode() %></h6>
                                    <!--                                    <a href="" class="btn-primary">Detail</a>-->

                                    <form val="SubjectListController" method="post" onsubmit="return confirmDelete();">

                                        <input type="hidden" name="subjectID" value="<%= s.getSubjectID() %>">
                                        <input type="hidden" name="classID" value="<%= cl.getClassID()%>">
                                        <input type="submit" class="btn btn-danger mb-5" value="Delete">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <% } %>
                        <% }else{ %>
                        <h1 style="color: red">No subject yet, Subject hasn't been added to this class!</h1>
                        <% } %>
                    </div>
                </div>
            </main>

        </div>

    </body>


</html>
