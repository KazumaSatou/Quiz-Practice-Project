<%-- 
    Document   : ChangePassword
    Created on : Jun 2, 2023, 4:07:24 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%String error = (String)request.getAttribute("error"); %>
<%String error1 = (String)request.getAttribute("error1"); %>
<%String error2 = (String)request.getAttribute("error2"); %>
<%String error3 = (String)request.getAttribute("error3"); %>
<%String error4 = (String)request.getAttribute("error4"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change PassWord</title>
        <style>
            .mainDiv {
                display: flex;
                min-height: 100%;
                align-items: center;
                justify-content: center;
                background-color: #f9f9f9;
                font-family: 'Open Sans', sans-serif;
            }
            .cardStyle {
                width: 500px;
                border-color: white;
                background: #fff;
                padding: 36px 0;
                border-radius: 4px;
                margin: 30px 0;
                box-shadow: 0px 0 2px 0 rgba(0,0,0,0.25);
            }
            #signupLogo {
                max-height: 100px;
                margin: auto;
                display: flex;
                flex-direction: column;
            }
            .formTitle{
                font-weight: 600;
                margin-top: 20px;
                color: #2F2D3B;
                text-align: center;
            }
            .inputLabel {
                font-size: 12px;
                color: #555;
                margin-bottom: 6px;
                margin-top: 24px;
            }
            .inputDiv {
                width: 70%;
                display: flex;
                flex-direction: column;
                margin: auto;
            }
            .inputDiv1 {
                width: 70%;
                display: flex;
                flex-direction: column;
                margin: auto;
                color: red;
                margin-right:  10px;
            }
            input {
                height: 40px;
                font-size: 16px;
                border-radius: 4px;
                border: none;
                border: solid 1px #ccc;
                padding: 0 11px;
            }
            input:disabled {
                cursor: not-allowed;
                border: solid 1px #eee;
            }
            .buttonWrapper {
                margin-top: 40px;
            }
            .submitButton {
                width: 70%;
                height: 40px;
                margin: auto;
                display: block;
                color: #fff;
                background-color: #065492;
                border-color: #065492;
                text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
                box-shadow: 0 2px 0 rgba(0, 0, 0, 0.035);
                border-radius: 4px;
                font-size: 14px;
                cursor: pointer;
            }
            .submitButton:disabled,
            button[disabled] {
                border: 1px solid #cccccc;
                background-color: #cccccc;
                color: #666666;
            }

            #loader {
                position: absolute;
                z-index: 1;
                margin: -2px 0 0 10px;
                border: 4px solid #f3f3f3;
                border-radius: 50%;
                border-top: 4px solid #666666;
                width: 14px;
                height: 14px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
            }

            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }
        </style>
    </head>
    <body>
       

        <!--        <form action="ChangePassController" method="POST" >
                    <input type="hidden" name="accountID" value="${currUser.getAccountID()}">
                    Current Password: <input type="password" name="password"><br>
                    New Password: <input type="password" name="newPassword"><br>
                    Confirm New Password: <input type="password" name="confirmNewPassword"><br>
                    <input type="submit" value="Change Password">
                </form>-->


        <div class="mainDiv">
            <div class="cardStyle">
                <form action="ChangePassController" method="post" name="signupForm" id="signupForm">

                    <img src="" id="signupLogo"/>

                    <h2 class="formTitle">
                        Change Password
                    </h2>

                    <div>
                        <input type="hidden" name="accountID" value="${currUser.getAccountID()}">
                    </div>

                    <div class="inputDiv">
                        <label class="inputLabel" for="password">Current Password</label>
                        <input type="password" id="password" name="password" oninvalid="Please fill in this field" required>
                    </div>
                    <div class="inputDiv">
                        <label class="inputLabel" for="newPassword">New Password</label>
                        <input type="password" id="newPassword" name="newPassword" oninvalid="Please fill in this field" required>
                    </div>

                    <div class="inputDiv">
                        <label class="inputLabel" for="confirmNewPassword">Confirm Password</label>
                        <input type="password" id="confirmNewPassword" name="confirmNewPassword" oninvalid="Please fill in this field" required>
                    </div>

                    <div class="inputDiv1">
                        <% if (request.getParameter("error") != null) {
                        int errorCode = Integer.parseInt(request.getParameter("error"));
                        switch (errorCode) {
                            case 0:
                                out.println("<p>Change Password successful.</p>");
                                break;
                            case 1:
                                out.println("<p>New password and confirm password do not match.</p>");
                                break;
                            case 2:
                                out.println("<p>New password must not be less than 6 characters.</p>");
                                break;
                            case 3:
                                out.println("<p>Current password is incorrect.</p>");
                                break;
                            case 4:
                                out.println("<p>Error updating password.</p>");
                                break;
                            case 5:
                                out.println("<p> Handling database access errors</p>");
                                break;
                        }
                        } %>
                    </div>
                    <div class="buttonWrapper">
                        <button type="submit" id="submitButton" onclick="validateSignupForm()" class="submitButton pure-button pure-button-primary">
                            <span>Change Password</span>
                            <!--                                                    <span id="loader"></span>-->
                        </button>
                       
                    </div>



                </form>
            </div>
        </div>

    </body>
</html>
