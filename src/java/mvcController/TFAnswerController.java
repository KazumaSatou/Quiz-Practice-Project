/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.AccountDBContext;
import dao.DAOStudent;
import entity.Answer;
import entity.Question;
import entity.Student;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author FPT
 */
public class TFAnswerController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int QuizID = Integer.parseInt(request.getParameter("quizID"));
        int trueAnswerCount = (int) request.getAttribute("trueAnswerCount");
        float quizResult = (float) request.getAttribute("quizResult");
        DAOStudent dao = new DAOStudent();

        List<Question> listQuestion = dao.getQuestionByQuizID(QuizID);

        List<Answer> listAllAnswers = dao.getAllAnswer();
        String time = dao.getTimeByQuizID(QuizID);
//        for (Question question : listQuestion) {
//            dao.createTestDetail(question.getQuestionID(), Integer.parseInt(QuizID));
//        }
        if (listQuestion != null) {
            request.setAttribute("time", time);
            request.setAttribute("listQuestion", listQuestion);
            request.setAttribute("listAllAnswers", listAllAnswers);
            request.setAttribute("quizID", QuizID);
//            Khi muốn chuyển sang kèm theo dữ liệu thì dùng request.getRequestDispatcher().forward()
            Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if (foundUser == null || foundUser.getRoleID() != 2) {
                response.sendRedirect("LoginController");
                return;
            }
            request.getRequestDispatcher("StudentScore.jsp").forward(request, response);
        } else {
//            Khi điều hướng chuyển trang mà ko cần dữ liệu thì dùng response.sendRedirect
            response.sendRedirect(request.getContextPath() + "/testLoginController");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
