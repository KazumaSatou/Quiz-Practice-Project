
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOStudent;
import dao.DAOSubject;
import entity.Quiz;
import entity.Class;
import entity.Student;
import entity.Subject;
import entity.Users;
//import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */

public class StudentViewEnrollSubject extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StudentViewEnrollSubject</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StudentViewEnrollSubject at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if(foundUser == null || foundUser.getRoleID() != 2){
                response.sendRedirect("LoginController");
                return;
            }

         String studentID = request.getParameter("StudentID");
        String SubjectID = request.getParameter("SubjectID");
        DAOStudent dao = new DAOStudent();
        if (studentID != null) {
            
            Student stu = dao.getStudentByAccountId(Integer.parseInt(studentID));

            List<Subject> sub = dao.getSubjectByStudentID(stu.getAccountID());
            List<Class> cl = dao.getClassByStudentID(stu.getAccountID());
            if (sub != null) {
                request.setAttribute("listClass", cl);
                request.setAttribute("listSub11", sub);
//            Khi muốn chuyển sang kèm theo dữ liệu thì dùng request.getRequestDispatcher().forward()
                request.getRequestDispatcher("StudentViewSubject.jsp").forward(request, response);
            } else {
//            Khi điều hướng chuyển trang mà ko cần dữ liệu thì dùng response.sendRedirect
                response.sendRedirect(request.getContextPath() + "/testLoginController");
            }
        } else if (SubjectID != null) {

            List<Quiz> quiz = dao.getAllQuizBySubjectID(Integer.parseInt(SubjectID));

            if (quiz != null) {
                request.setAttribute("listQuiz", quiz);
//            Khi muốn chuyển sang kèm theo dữ liệu thì dùng request.getRequestDispatcher().forward()
                request.getRequestDispatcher("StudentViewQuiz.jsp").forward(request, response);
            } else {
//            Khi điều hướng chuyển trang mà ko cần dữ liệu thì dùng response.sendRedirect
//                response.sendRedirect(request.getContextPath() + "/testLoginController");
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

