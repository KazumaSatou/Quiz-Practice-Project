/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOQuiz;
import entity.Quiz;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author dell
 */
public class CreateQuizController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateQuizController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateQuizController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Users foundUser = (Users) request.getSession().getAttribute("currUser");
        if (foundUser == null || foundUser.getRoleID() != 3) {
            response.sendRedirect("LoginController");
            return;
        }
        request.getRequestDispatcher("CreateQuiz.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String quizTitle = request.getParameter("quizTitle");
        String description = request.getParameter("description");
        int numberQuestion = Integer.parseInt(request.getParameter("numberQuestion"));
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String times = request.getParameter("times");
        int subjectID = Integer.parseInt(request.getParameter("subjectID"));

        // Tạo đối tượng Quiz và gán giá trị
        Quiz quiz = new Quiz();
        quiz.setQuizTitle(quizTitle);
        quiz.setDescription(description);
        quiz.setNumberQuestion(numberQuestion);
        quiz.setStartDate(startDate);
        quiz.setEndDate(endDate);
        quiz.setTimes(times);
        quiz.setSubjectId(subjectID);
        DAOQuiz daoQuiz = new DAOQuiz();
        try {
            // Thêm bài thi vào cơ sở dữ liệu
            daoQuiz.createQuiz(quiz);
            int n = daoQuiz.getQuizID(quizTitle, description, numberQuestion, startDate, endDate, times, subjectID);
            quiz.setQuizId(n);
//            HttpSession session = request.getSession();
//            session.setAttribute("quiz", quiz);
            request.setAttribute("quizId", n);
            request.setAttribute("subjectId", subjectID);
            String redirectURL = "AddQuestionToTestController?quizId=" + quiz.getQuizId() + "&subjectId=" + quiz.getSubjectId();
            response.sendRedirect(redirectURL);
        } catch (SQLException ex) {
            // Gửi thông báo lỗi đến trình duyệt
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.println("<h2>An error occurred while creating the quiz</h2>");
            out.println("<p>Error details: " + ex.getMessage() + "</p>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
