/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOSubject;
import dao.AccountDBContext;
import dao.DAOClass;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 *
 * @author FPT
 */
public class CreateClassController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateClassController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateClassController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if(foundUser == null || foundUser.getRoleID() != 1){
                response.sendRedirect("LoginController");
                return;
            }
        request.getRequestDispatcher("createClass.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String classID = request.getParameter("classID");
        String account = request.getParameter("Account");
        String semester = request.getParameter("Semester");
        String phoneRegex = "^\\d+$";
        String usernameRegex = "^[a-zA-Z0-9]+$";
        boolean isValid = true;
        String errorMessage = "";
        AccountDBContext daovip = new AccountDBContext();
        DAOClass dao = new DAOClass();
        int n = daovip.getIDByAcc(account);
        int m = daovip.getTeacherIDByAccID(n);
        String me = Integer.toString(m);

        if (classID == null || !Pattern.matches(usernameRegex, classID)) {
            isValid = false;
            errorMessage += "Invalid format. Class ID must contain only numbers.<br>";
            request.setAttribute("error1", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (dao.getClasstById(classID) != null){
            isValid = false;
            errorMessage += "Class ID was used!";
            request.setAttribute("error1", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (account == null || !Pattern.matches(usernameRegex, account)) {
            isValid = false;
            errorMessage += "Invalid format. Account must contain only words and numbers.<br>";
            request.setAttribute("error2", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (daovip.getAccByAcc(account) == null) {
            isValid = false;
            errorMessage += "Account not available!<br>";
            request.setAttribute("error2", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (semester == null || !Pattern.matches(usernameRegex, semester)) {
            isValid = false;
            errorMessage += "Invalid format. Subject ID must contain only numbers.<br>";
            request.setAttribute("error3", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (isValid) {
            dao.createClass(classID, me, semester);
            response.sendRedirect("ViewClassController");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
