/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import entity.Class;
import dao.DAOClass;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 *
 * @author FPT
 */
public class ViewClassController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DAOClass daoClass = new DAOClass();
        Map<String, List<?>> dataMap = daoClass.getAllClassAndAccount();

        List<Class> classList = (List<Class>) dataMap.get("classes");
        List<Users> accountList = (List<Users>) dataMap.get("accounts");

        request.setAttribute("classList", classList);
        request.setAttribute("accountList", accountList);
         Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if(foundUser == null || foundUser.getRoleID() != 1){
                response.sendRedirect("LoginController");
                return;
            }

        request.getRequestDispatcher("adminClass.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String classID = request.getParameter("id");
        DAOClass dao = new DAOClass();
        Class c = dao.getClasstById(classID);
        out.println("<div class=\"col-6\">\n"
                + "                                <label for=\"exampleInputEmail1\">Teacher name</label>\n"
                + " <input type=\"hidden\" name=\"classID\" value=\"" + c.getClassID() + "\"/>\n"
                + "                                <input\n"
                + "                                    name=\"teacherID\"\n"
                + "                                    type=\"text\"\n"
                + "                                    class=\"form-control\"\n"
                + "                                    id=\"exampleInputEmail1\"\n"
                + "                                    aria-describedby=\"emailHelp\"\n"
                + "                                    value=\"" + c.getTeacherID() + "\"\n"
                + "                                    />               \n"
                + "                            </div>\n"
                + "                            <div class=\"col-6\">\n"
                + "                                <label for=\"exampleInputEmail1\">Semester</label>\n"
                + "                                <input\n"
                + "                                    name=\"semester\"\n"
                + "                                    type=\"text\"\n"
                + "                                    class=\"form-control\"\n"
                + "                                    id=\"exampleInputEmail1\"\n"
                + "                                    aria-describedby=\"emailHelp\"\n"
                + "                                    value=\"" + c.getSemester() + "\"\n"
                + "                                    />               \n"
                + "                            </div>\n");
        out.println("<div class=\"col-12\" style=\"margin-left: 34%; margin-top: 4%\">\n"
                + "                                <button type=\"submit\" class=\"btn btn-primary\">Save changes</button>\n"
                + "                            </div>");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
