/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOUsers;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import static java.lang.System.out;
import java.util.regex.Pattern;

/**
 *
 * @author Admin
 */
public class SignUpController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        try ( PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet SignUpController</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet SignUpController at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
        PrintWriter pr = response.getWriter();
        Users u = new Users();
        DAOUsers dao = new DAOUsers();

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        String confirmPassword = request.getParameter("confirmPassword");
        String email = request.getParameter("email");
        //String phone = request.getPhone("phone");
        String address = request.getParameter("address");
        String phoneNumber = request.getParameter("phoneNumber");

        // Kiểm tra ràng buộc dữ liệu
        boolean isValid = true;
        String errorMessage = "";

        // Kiểm tra tên đăng nhập không được trống và chỉ chứa các ký tự chữ và số
        String usernameRegex = "^[a-zA-Z0-9]+$";
        if (username == null || !Pattern.matches(usernameRegex, username)) {
            isValid = false;
//            pr.print("Invalid username format. Username must contain only letters and numbers.");
//            request.getRequestDispatcher("SignUp.jsp").include(request, response);
            errorMessage += "Invalid username format. Username must contain only letters and numbers.<br>";
            request.setAttribute("error1", errorMessage);
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        }
        // Kiểm tra tên không được trống
        if (username == null || username.trim().isEmpty()) {
            isValid = false;
            errorMessage += "Name is required.<br>";
            request.setAttribute("error2", errorMessage);
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        }

        // Kiểm tra định dạng email hợp lệ
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        if (email == null || !Pattern.matches(emailRegex, email)) {
            isValid = false;
            errorMessage += "Invalid email format.<br>";
            request.setAttribute("error3", errorMessage);
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        }
        boolean isValidEmail = email.endsWith("fpt.edu.vn");
        if (!isValidEmail) {
            errorMessage += "Email is invalid.";
            request.setAttribute("error4", errorMessage);
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        }

        // Kiểm tra mật khẩu không được trống và độ dài tối thiểu là 6 ký tự
        if (password == null || password.length() < 6) {
            isValid = false;
            errorMessage += "Password must be at least 6 characters.<br>";
            request.setAttribute("error5", errorMessage);
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        }
        if (!password.equals(confirmPassword)) {
            response.getWriter().println("Password and confirmation password do not match!");
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
            return;
        }

        String phoneRegex = "^0\\d{9}$";
        if (phoneNumber == null || !Pattern.matches(phoneRegex, phoneNumber)) {
            isValid = false;
            errorMessage += "Invalid phone format.<br>";
            request.setAttribute("error6", errorMessage);
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        }
        // Xử lý kết quả đăng ký
        if (isValid) {
            // Thực hiện lưu thông tin đăng ký vào cơ sở dữ liệu hoặc thực hiện các tác vụ khác
            // ...
            out.println("Registration successful for user: " + username);
        } else {
            out.println("<span style='color: red;'>Registration failed:</span><br>" + errorMessage);
        }
        response.sendRedirect("Login.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
