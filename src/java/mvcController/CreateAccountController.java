/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.AccountDBContext;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 *
 * @author Admin
 */
public class CreateAccountController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet createAccount</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet createAccount at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if(foundUser == null || foundUser.getRoleID() != 1){
                response.sendRedirect("LoginController");
                return;
            }//filter
        request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String account = request.getParameter("account");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        String fullname = request.getParameter("fullname");
        String age = request.getParameter("age");
        String gender = request.getParameter("gender");
        boolean isValid = true;
        String errorMessage = "";
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        String phoneRegex = "^0\\d{9}$";
        AccountDBContext dao = new AccountDBContext();
        request.setAttribute("enteredAccountValue", account);
        request.setAttribute("enteredPasswordValue", password);
        request.setAttribute("enteredEmailValue", email);
        request.setAttribute("enteredPhoneValue", phone);
        request.setAttribute("enteredAddressValue", address);
        request.setAttribute("enteredFullnameValue", fullname);
        request.setAttribute("enteredAgeValue", age);

        // Kiểm tra tên đăng nhập không được trống và chỉ chứa các ký tự chữ và số
//        String usernameRegex = "^[a-zA-Z0-9]+$";
//        if (account == null || !Pattern.matches(usernameRegex, account)) {
//            isValid = false;
//            errorMessage += "Invalid username format. Username must contain only letters and numbers.<br>";
//            request.setAttribute("error1", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//        if (dao.getAccByAcc(account) != null) {
//            isValid = false;
//            errorMessage += "Account already used!<br>";
//            request.setAttribute("error1", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//        // Kiểm tra định dạng email hợp lệ
//        if (email == null || !Pattern.matches(emailRegex, email)) {
//            isValid = false;
//            errorMessage += "Invalid email format.<br>";
//            request.setAttribute("error3", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//
//        if (dao.getUserBEmail(email) != null) {
//            isValid = false;
//            errorMessage += "Email already used!<br>";
//            request.setAttribute("error3", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//        if (role.equals("2")) {
//            isValid = false;
//            boolean isValidEmail = email.endsWith("fpt.edu.vn");
//            if (!isValidEmail) {
//
//                errorMessage += "Student's email must end with the extension fpt.edu.vn!";
//                request.setAttribute("error3", errorMessage);
//                request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//            }
//        }
//        if (role.equals("3")) {
//            isValid = false;
//            boolean isValidEmail = email.endsWith("fe.edu.vn");
//            if (!isValidEmail) {
//                errorMessage += "Teacher's email must end with the extension fe.edu.vn!";
//                request.setAttribute("error3", errorMessage);
//                request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//            }
//        }
//
//        //Kiem tra sdt
//        if (phone == null || !Pattern.matches(phoneRegex, phone)) {
//            isValid = false;
//            errorMessage += "Invalid phone format. Please enter the number<br>";
//            request.setAttribute("error6", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//        if (address == null || address.trim().isEmpty()) {
//            isValid = false;
//            errorMessage += "Address is required.<br>";
//            request.setAttribute("error7", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//        if (password == null || password.length() < 6) {
//            isValid = false;
//            errorMessage += "Password must be at least 6 characters.<br>";
//            request.setAttribute("error2", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//
//        // Kiểm tra tên không được trống
//        if (fullname == null || fullname.trim().isEmpty()) {
//            isValid = false;
//            errorMessage += "Full name is required.<br>";
//            request.setAttribute("error8", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//        if (age == null || age.trim().isEmpty()) {
//            isValid = false;
//            errorMessage += "Age is required.<br>";
//            request.setAttribute("error9", errorMessage);
//            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
//        }
//        if (isValid) {
//            dao.createAccount(account, email, address, phone, password, role, fullname, age, gender);
//            response.sendRedirect("adminHomePage");
//        }
//        dao.createAccount(account, email, address, phone, password, role, fullname, age, gender);
//        response.sendRedirect("adminHomePage");
// Kiểm tra tên đăng nhập
        String usernameRegex = "^[a-zA-Z0-9]+$";
        if (account == null || !Pattern.matches(usernameRegex, account)) {
            isValid = false;
            errorMessage += "Invalid username format. Username must contain only letters and numbers.<br>";
            request.setAttribute("error1", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        } else if (dao.getAccByAcc(account) != null) {
            isValid = false;
            errorMessage += "Account already used!<br>";
            request.setAttribute("error1", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

        // Kiểm tra định dạng email
        if (email == null || !Pattern.matches(emailRegex, email)) {
            isValid = false;
            errorMessage += "Invalid email format.<br>";
            request.setAttribute("error3", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        } else if (dao.getUserBEmail(email) != null) {
            isValid = false;
            errorMessage += "Email already used!<br>";
            request.setAttribute("error3", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        } else if (role.equals("2") && !email.endsWith("fpt.edu.vn")) {
            isValid = false;
            errorMessage += "Student's email must end with the extension fpt.edu.vn!<br>";
            request.setAttribute("error3", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        } else if (role.equals("3") && !email.endsWith("fe.edu.vn")) {
            isValid = false;
            errorMessage += "Teacher's email must end with the extension fe.edu.vn!<br>";
            request.setAttribute("error3", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

        // Kiểm tra số điện thoại
        if (phone == null || !Pattern.matches(phoneRegex, phone)) {
            isValid = false;
            errorMessage += "Invalid phone format. Please enter a valid phone number.<br>";
            request.setAttribute("error6", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

        // Kiểm tra địa chỉ
        if (address == null || address.trim().isEmpty()) {
            isValid = false;
            errorMessage += "Address is required.<br>";
            request.setAttribute("error7", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

        // Kiểm tra mật khẩu
        if (password == null || password.length() < 6) {
            isValid = false;
            errorMessage += "Password must be at least 6 characters.<br>";
            request.setAttribute("error2", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

        // Kiểm tra tên đầy đủ
        if (fullname == null || fullname.trim().isEmpty()) {
            isValid = false;
            errorMessage += "Full name is required.<br>";
            request.setAttribute("error8", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

        // Kiểm tra tuổi
        if (age == null || age.trim().isEmpty()) {
            isValid = false;
            errorMessage += "Age is required.<br>";
            request.setAttribute("error9", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

        // Kiểm tra có lỗi hay không
        if (isValid) {
            dao.createAccount(account, email, address, phone, password, role, fullname, age, gender);
            response.sendRedirect("adminHomePage");
        } else {
            // Chuyển hướng về trang createTeacher.jsp với thông báo lỗi
            request.setAttribute("error", errorMessage);
            request.getRequestDispatcher("createTeacher.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
