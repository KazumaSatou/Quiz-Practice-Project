/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOQuestion;
import dao.DAOStudent;
import entity.Answer;
import entity.Question;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class QuestionBankController extends HttpServlet {

//    private DAOQuestion daoQuestion;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuestionBankController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuestionBankController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Users foundUser = (Users) request.getSession().getAttribute("currUser");
        if (foundUser == null || foundUser.getRoleID() != 3) {
            response.sendRedirect("LoginController");
            return;
        }
        DAOQuestion daoQuestion = new DAOQuestion();
        DAOStudent daoStu = new DAOStudent();
        String action = request.getParameter("action");

        // Mặc định hiển thị danh sách câu hỏi trong question bank
        int chapterID = Integer.parseInt(request.getParameter("chapterID"));

        List<Question> questions = daoQuestion.getAllQuestionsByChapter(chapterID);
        List<Answer> listAllAnswers = daoStu.getAllAnswer();

        request.setAttribute("questions", questions);
        request.setAttribute("answers", listAllAnswers);
        request.getRequestDispatcher("QuestionBank1.jsp").forward(request, response);

//        } else if (action.equals("viewAnswer")) {
//            int questionID = Integer.parseInt(request.getParameter("questionID"));
//            int chapterID = Integer.parseInt(request.getParameter("chapterID"));
//            List<Answer> list = daoQuestion.getAllAnswerByQuestion(questionID);
//            response.sendRedirect("QuestionBankController?chapterID=" + chapterID);
//        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        DAOQuestion daoQuestion = new DAOQuestion();
        String action = request.getParameter("action");

        if (action != null && action.equals("addQuestion")) {
            int chapterID = Integer.parseInt(request.getParameter("chapterID"));
            String content = request.getParameter("content").trim();

            if (content.isEmpty()) {
                // Thiết lập thông báo lỗi
                String error = "Please enter the text of the question";

                // Đặt thông báo lỗi vào thuộc tính của request
                request.setAttribute("error", error);
                request.setAttribute("chapterID", chapterID);
                request.getRequestDispatcher("QuestionBank1.jsp").forward(request, response);
                //response.sendRedirect("QuestionBankController?chapterID=" + chapterID);
            } else {

                String[] answers = request.getParameterValues("answerContent");
                String[] answerPoint = request.getParameterValues("answerPoint");

                boolean hasError = false; // Biến kiểm tra lỗi

                // Kiểm tra trường "Answer" và "Answer Point" trống
                for (int i = 0; i < answers.length; i++) {
                    String answer = answers[i].trim();
                    String answerPoints = answerPoint[i].trim();

                    if (answer.isEmpty() || answerPoints.isEmpty()) {
                        hasError = true;
                        break; // Dừng kiểm tra nếu có lỗi
                    }
                }
                if (hasError) {
                    // Thiết lập thông báo lỗi
                    String error1 = "Please fill in all the information for the answer and score";

                    // Đặt thông báo lỗi vào thuộc tính của request
                    request.setAttribute("error1", error1);
                    request.setAttribute("chapterID", chapterID);

                    // Chuyển hướng trang về QuestionBank1.jsp
                    request.getRequestDispatcher("QuestionBank1.jsp").forward(request, response);
                } else {
                    daoQuestion.insertQuestion(content, chapterID);
                    int n = daoQuestion.getQuestionID(content, chapterID);
                    boolean inCorrect = false;
                    for (int i = 0; i < answers.length; i++) {
                        if (!answerPoint[i].equals("0") && answerPoint[i] != null) {
                            inCorrect = true;
                            //out.println("<p>" + answers[i] + "-" + answerPoint[i] + "-" + inCorrect + "</p>");
                            daoQuestion.insertAnswer(answers[i], inCorrect, Float.parseFloat(answerPoint[i]), n);
                        } else {
                            inCorrect = false;
                            //out.println("<p>" + answers[i] + "-" + answerPoint[i] + "-" + inCorrect + "</p>");
                            daoQuestion.insertAnswer(answers[i], inCorrect, Float.parseFloat(answerPoint[i]), n);
                        }
                    }
                    response.sendRedirect("QuestionBankController?chapterID=" + chapterID);
                }

            }
        } else if (action != null && action.equals("DeleteQuestion")) {
            int questionID = Integer.parseInt(request.getParameter("questionID"));
            int chapterID = Integer.parseInt(request.getParameter("chapterID"));
            daoQuestion.deleteAnswernew(questionID);
            daoQuestion.deleteTest(questionID);

            daoQuestion.deleteQuestion1(questionID);
            response.sendRedirect("QuestionBankController?chapterID=" + chapterID);
//                response.sendRedirect("QuestionBankController?chapterID=" + chapterID);
        } else if (action != null && action.equals("searchQuestions")) {
            String keyword = request.getParameter("keyword");
            int chapterID = Integer.parseInt(request.getParameter("chapterID"));
            DAOStudent daoStu = new DAOStudent();
            List<Question> searchResults = daoQuestion.searchQuestions(keyword,chapterID);
            List<Answer> listAllAnswers = daoStu.getAllAnswer();

            request.setAttribute("answers", listAllAnswers);
            request.setAttribute("questions", searchResults);
            request.getRequestDispatcher("QuestionBank1.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
