/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOClass;
import dao.DAOSubject;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author DELL
 */
public class AddSubjectController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddSubjectController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSubjectController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String classID = request.getParameter("classID");
        String subjectID = request.getParameter("subjectID");
        DAOSubject dao1 = new DAOSubject();
        DAOClass dao = new DAOClass();
        boolean isValid = true;
        String subjectRegex = "^[a-zA-Z0-9\\s\\-\\.]+$";
        String errorMessage = "";
        if (classID == null || subjectID.isEmpty()) {

            isValid = false;
            errorMessage += "Subject ID must be not empty<br>";
            request.setAttribute("err2", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (subjectID == null || classID.isEmpty()) {
            isValid = false;
            errorMessage += "Class ID must be not empty.<br>";
            request.setAttribute("err1", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (dao.getClasstById(classID) == null) {
            isValid = false;
            errorMessage += "Class ID is not exist";
            request.setAttribute("err1", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (dao1.getSubjectString(subjectID) == null) {
            isValid = false;
            errorMessage += "Subject ID is not exist";
            request.setAttribute("err2", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }
        if (isValid = true) {
            dao.addSubjectToClass(classID, subjectID);
            response.sendRedirect("SubjectListController?classID=" + classID);
        } else {
            request.setAttribute("error", errorMessage);
            request.getRequestDispatcher("createClass.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
