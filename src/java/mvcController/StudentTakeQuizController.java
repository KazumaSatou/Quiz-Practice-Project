/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.AccountDBContext;
import dao.DAOStudent;
import entity.Answer;
import entity.Question;
import entity.Student;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author Admin
 */
public class StudentTakeQuizController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StudentTakeQuizController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StudentTakeQuizController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);

        int QuizID = Integer.parseInt(request.getParameter("QuizID"));

        DAOStudent dao = new DAOStudent();

        List<Question> listQuestion = dao.getQuestionByQuizID(QuizID);

        List<Answer> listAllAnswers = dao.getAllAnswer();
        String time = dao.getTimeByQuizID(QuizID);
//        for (Question question : listQuestion) {
//            dao.createTestDetail(question.getQuestionID(), Integer.parseInt(QuizID));
//        }
        if (listQuestion != null) {
            request.setAttribute("time", time);
            request.setAttribute("listQuestion", listQuestion);
            request.setAttribute("listAllAnswers", listAllAnswers);
            request.setAttribute("quizID", QuizID);
//            Khi muốn chuyển sang kèm theo dữ liệu thì dùng request.getRequestDispatcher().forward()
            Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if (foundUser == null || foundUser.getRoleID() != 2) {
                response.sendRedirect("LoginController");
                return;
            }
            request.getRequestDispatcher("StudentTakeQuiz.jsp").forward(request, response);
        } else {
//            Khi điều hướng chuyển trang mà ko cần dữ liệu thì dùng response.sendRedirect
            response.sendRedirect(request.getContextPath() + "/testLoginController");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        int quizID = Integer.parseInt(request.getParameter("quizID"));
        String[] Size = request.getParameterValues("lengthOfQuiz");
        int count = 0;
        DAOStudent dao = new DAOStudent();
        for (int i = 0; i < Size.length; i++) {
            List<Answer> list = dao.getMultiAnswersByQuestionId(Integer.parseInt(Size[i]));
            String[] selectedAnswers = request.getParameterValues("Answer" + Size[i]);

            if (selectedAnswers != null) {
                boolean allAnswersCorrect = true;

                for (String selectedAnswer : selectedAnswers) {
                    boolean isAnswerCorrect = false;

                    for (Answer answer : list) {
                        if (selectedAnswer.equals(answer.getAnswerText())) {
                            isAnswerCorrect = true;
                            break; // Break out of the loop after finding a match
                        }
                    }

                    if (!isAnswerCorrect) {
                        allAnswersCorrect = false;
                        break; // Break out of the loop if any answer is incorrect
                    }
                }

                if (allAnswersCorrect) {
                    count++;
                }
            }
        }
        float result = ((float) count / (float) Size.length) * 10;
        HttpSession session = request.getSession();
        List<String> studentList = (List<String>) session.getAttribute("student");
        String accountID = studentList.get(0);
        AccountDBContext daoo = new AccountDBContext();
        int sID = daoo.getStudentIDByAccID(accountID);
        dao.addPoint(result, sID, quizID);
        // Set the count and result as attributes in the request
        request.setAttribute("trueAnswerCount", count);
        request.setAttribute("quizResult", result);
        request.setAttribute("quizID", quizID);
        // Forward the request to another servlet or JSP for display
        request.getRequestDispatcher("TFAnswerController").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
