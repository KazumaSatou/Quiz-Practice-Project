/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.AccountDBContext;
import entity.Student;
import entity.Teacher;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author Admin
 */
public class HomePageAdmin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if(foundUser == null || foundUser.getRoleID() != 1){
                response.sendRedirect("LoginController");
                return;
            }//filter phan quyen
            AccountDBContext adc = new AccountDBContext();
            List<Users> list = adc.getAllUsers();
            request.setAttribute("list", list);
            request.getRequestDispatcher("admin.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String id = request.getParameter("id");
        AccountDBContext adc = new AccountDBContext();
        Users u = adc.getUsersById(Integer.parseInt(id));
        if (u.getRoleID() == 2) {
            Student s = adc.getStudentById(Integer.parseInt(id));
            out.println("<div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Account</label>\n"
                    + " <input type=\"hidden\" name=\"id\" value=\"" + u.getAccountID() + "\"/>\n"
                    + "                                <input\n"
                    + "                                    name=\"account\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getAccount() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Email</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"email\"\n"
                    + "                                    type=\"email\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getEmail() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Phone</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"phone\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getPhone() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Address</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"address\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getAddress()+ "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">FullName</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"fullname\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + s.getName() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Age</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"age\"\n"
                    + "                                    type=\"number\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + s.getAge() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Gender</label>\n"
                    + "                                <select name=\"gender\" class=\"form-control\">\n");
            if (s.getGender().equals("0")) {
                out.println("<option value=\"1\" selected>Male</option>\n"
                        + "                                    <option value=\"2\">Female</option>\n");
            } else {
                out.println("<option value=\"1\">Male</option>\n"
                        + "                                    <option value=\"2\" selected>Female</option>\n");
            }
            out.println("                                </select>           \n"
                    + "                            </div>");
        } else {
            Teacher s = adc.getTeacherById(Integer.parseInt(id));
            out.println("<div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Account</label>\n"
                    + " <input type=\"hidden\" name=\"id\" value=\"" + u.getAccountID() + "\"/>\n"
                    + "                                <input\n"
                    + "                                    name=\"account\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getAccount() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Email</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"email\"\n"
                    + "                                    type=\"email\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getEmail() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Phone</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"phone\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getPhone() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Address</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"address\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + u.getAddress()+ "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">FullName</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"fullname\"\n"
                    + "                                    type=\"text\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + s.getName() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Age</label>\n"
                    + "                                <input\n"
                    + "                                    name=\"age\"\n"
                    + "                                    type=\"number\"\n"
                    + "                                    class=\"form-control\"\n"
                    + "                                    id=\"exampleInputEmail1\"\n"
                    + "                                    aria-describedby=\"emailHelp\"\n"
                    + "                                    value=\"" + s.getAge() + "\"\n"
                    + "                                    />               \n"
                    + "                            </div>\n"
                    + "                            <div class=\"col-6\">\n"
                    + "                                <label for=\"exampleInputEmail1\">Gender</label>\n"
                    + "                                <select name=\"gender\" class=\"form-control\">\n");
            if (s.getGender().equals("0")) {
                out.println("<option value=\"1\" selected>Male</option>\n"
                        + "                                    <option value=\"2\">Female</option>\n");
            } else {
                out.println("<option value=\"1\">Male</option>\n"
                        + "                                    <option value=\"2\" selected>Female</option>\n");
            }
            out.println("                                </select>           \n"
                    + "                            </div>");
        }
        out.println("<div class=\"col-12\" style=\"margin-left: 34%; margin-top: 4%\">\n"
                    + "                                <button type=\"submit\" class=\"btn btn-primary\">Save changes</button>\n"
                    + "                            </div>");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
