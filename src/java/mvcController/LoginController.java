/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.AccountDBContext;
import dao.DAOClass;
import dao.DAOUsers;
import entity.Users;
import entity.Class;
import entity.Teacher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        try ( PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet LoginController</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");

        PrintWriter pr = response.getWriter();
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        AccountDBContext daovip = new AccountDBContext();
        DAOUsers dao = new DAOUsers();
        Users foundUser = dao.getUser(account, password);
        HttpSession session = request.getSession(true);
        request.getSession().setAttribute("currUser", foundUser);   // Save current user's information to session
        String errorMessage = "";
        if (foundUser != null) {
            if (foundUser.getRoleID() == 1) {
                response.sendRedirect("adminHomePage");
            }
            if (foundUser.getRoleID() == 2) {
                List<String> list = new ArrayList<String>();
                list.add(Integer.toString(foundUser.getAccountID()));
                list.add(foundUser.getAccount());
                request.getSession().setAttribute("student", list);

                response.sendRedirect("HomePageStudent.jsp");
            }
            if (foundUser.getRoleID() == 3) {
                session.setAttribute("account", account);
                response.sendRedirect("ViewTeacherSubjectController");
            }
        } else {
//            pr.print("<h3>Invalid account or password!<h3>");
            errorMessage += "Invalid account or password!";
            request.setAttribute("error", errorMessage);
            request.getRequestDispatcher("Login.jsp").include(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
