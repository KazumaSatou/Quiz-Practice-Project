/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOUsers;
import entity.Teacher;
import entity.Class;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author Admin
 */
public class HomePageTeacher extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HomePageTeacher</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HomePageTeacher at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Lấy teacherID từ tham số truy vấn
        int teacherID = Integer.parseInt(request.getParameter("teacherID"));

        // Gọi DAO để lấy danh sách lớp học dựa trên teacherID
        DAOUsers classDAO = new DAOUsers();
        List<Class> classes = classDAO.getClassesByTeacherID(teacherID);

        // Lưu danh sách lớp học vào attribute và chuyển hướng sang trang "HomePageTeacher.jsp"
        request.setAttribute("classes", classes);
        request.getRequestDispatcher("HomePageTeacher.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
//        response.setContentType("text/html;charset=UTF-8");
//        PrintWriter pr = response.getWriter();
//        DAOUsers dao = new DAOUsers();
//        HttpSession session = request.getSession();
//        Teacher teacher = (Teacher) session.getAttribute("teacher");
//        if (teacher != null) {
//            int teacherID = teacher.getTeacherID();
//
//            List<entity.Class> classes = dao.getClassesByTeacherID(teacherID);
//            // Truyền danh sách lớp học sang JSP
//            request.setAttribute("classes", classes);
//
//            // Chuyển hướng tới trang JSP
//            RequestDispatcher dispatcher = request.getRequestDispatcher("HomePageTeacher.jsp");
//            dispatcher.forward(request, response);
//
//        } else {
//
//        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
