/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import entity.Test;
import entity.Question;
import dao.DAOQuestion;
import dao.DAOQuiz;
import entity.Chapter;
import entity.Quiz;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author FPT
 */
public class AddQuestionToTestController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int quizID = Integer.parseInt(request.getParameter("quizId"));
        int subjectID =  Integer.parseInt(request.getParameter("subjectId"));
        DAOQuestion dao = new DAOQuestion();
        List<Question> list = dao.getAllQuestion();
        List<Chapter> listChapter = dao.getAllChapter();
        DAOQuiz daonat = new DAOQuiz();
        Quiz q = daonat.getQuizByIdt(quizID);
        List<Question> load = daonat.getQuizAndQuestion(quizID);
        request.setAttribute("lmao", listChapter);
        request.setAttribute("load", load);
        request.setAttribute("list", list);
        request.setAttribute("quiz", q);
         Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if(foundUser == null || foundUser.getRoleID() != 3){
                response.sendRedirect("LoginController");
                return;
            }
        request.getRequestDispatcher("Test.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int subjectID =  Integer.parseInt(request.getParameter("subjectId"));
        int quizID = Integer.parseInt(request.getParameter("quizId"));
        int questionID = Integer.parseInt(request.getParameter("questionId"));
        Quiz quiz = new Quiz();
        quiz.setQuizId(quizID);
        quiz.setSubjectId(subjectID);
        DAOQuestion dao = new DAOQuestion();
        dao.insertTest(quizID, questionID);
        response.sendRedirect("AddQuestionToTestController?quizId=" + quiz.getQuizId()+"&subjectId="+quiz.getSubjectId());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
