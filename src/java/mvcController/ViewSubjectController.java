/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOSubject;
import entity.Subject;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author FPT
 */
public class ViewSubjectController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if (foundUser == null || foundUser.getRoleID() != 1) {
                response.sendRedirect("LoginController");
                return;
            }//filter
            DAOSubject sub = new DAOSubject();
            List<Subject> list = sub.getAllSubject();
            request.setAttribute("list", list);
            request.getRequestDispatcher("adminSubject.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String subjectID = request.getParameter("subjectID");
        DAOSubject dao = new DAOSubject();
        Subject sb = dao.getSubjectById(Integer.parseInt(subjectID));
        out.println("<div class=\"col-6\">\n"
                + "                                <label for=\"exampleInputEmail1\">Subject Name</label>\n"
                + " <input type=\"hidden\" name=\"subjectID\" value=\"" + sb.getSubjectID() + "\"/>\n"
                + "                                <input\n"
                + "                                    name=\"subjectName\"\n"
                + "                                    type=\"text\"\n"
                + "                                    class=\"form-control\"\n"
                + "                                    id=\"exampleInputEmail1\"\n"
                + "                                    aria-describedby=\"emailHelp\"\n"
                + "                                    value=\"" + sb.getSubjectName() + "\"\n"
                + "                                    />               \n"
                + "                            </div>\n"
                + "                            <div class=\"col-6\">\n"
                + "                                <label for=\"exampleInputEmail1\">Subject Code</label>\n"
                + "                                <input\n"
                + "                                    name=\"subjectCode\"\n"
                + "                                    type=\"text\"\n"
                + "                                    class=\"form-control\"\n"
                + "                                    id=\"exampleInputEmail1\"\n"
                + "                                    aria-describedby=\"emailHelp\"\n"
                + "                                    value=\"" + sb.getSubjectCode() + "\"\n"
                + "                                    />               \n"
                + "                            </div>\n");
        out.println("<div class=\"col-12\" style=\"margin-left: 34%; margin-top: 4%\">\n"
                + "                                <button type=\"submit\" class=\"btn btn-primary\">Save changes</button>\n"
                + "                            </div>");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
