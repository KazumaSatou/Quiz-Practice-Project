/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvcController;

import dao.DAOSubject;
import entity.Users;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 *
 * @author FPT
 */
public class CreateSubjectController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           Users foundUser = (Users) request.getSession().getAttribute("currUser");
            if(foundUser == null || foundUser.getRoleID() != 1){
                response.sendRedirect("LoginController");
                return;
            }
        request.getRequestDispatcher("createSubject.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String subjectName = request.getParameter("subjectName");
        String subjectCode = request.getParameter("subjectCode");
         // Loại bỏ dấu cách trong subjectName
        subjectName = subjectName.replaceAll("\\s+", "");
//        String subjectRegex = "^[a-zA-Z0-9]+$";
        String subjectRegex = "^[a-zA-Z0-9\\s\\-\\.]+$";
        DAOSubject subjectDAO = new DAOSubject();
        boolean isValid = true;
        String errorMessage = "";

        if (subjectName == null || !Pattern.matches(subjectRegex, subjectName)) {
            isValid = false;
            errorMessage += "Invalid format. Subject name must contain only letters and numbers.<br>";
            request.setAttribute("error1", errorMessage);
            request.getRequestDispatcher("createSubject.jsp").forward(request, response);
        }

        if (subjectCode == null || !Pattern.matches(subjectRegex, subjectCode)) {
            isValid = false;
            errorMessage += "Invalid format. Subject code must contain only letters and numbers.<br>";
            request.setAttribute("error2", errorMessage);
            request.getRequestDispatcher("createSubject.jsp").forward(request, response);
        }

        // Kiểm tra sự tồn tại của môn học
        if (subjectDAO.getSubjectByCode(subjectCode) != null) {
            isValid = false;
            errorMessage += "Subject code is already used!<br>";
            request.setAttribute("error2", errorMessage);
            request.getRequestDispatcher("createSubject.jsp").forward(request, response);
        }

        // Nếu môn học hợp lệ và chưa tồn tại, lưu vào cơ sở dữ liệu
        if (isValid) {
            subjectDAO.createSubject(subjectName, subjectCode);
            response.sendRedirect("ViewSubjectController");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
