/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Chapter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.DBConnect;

/**
 *
 * @author Admin
 */
public class DAOChapter extends DBConnect {

    public List<Chapter> getChaptersBySubjectID(int subjectID) throws SQLException {
        List<Chapter> chapterList = new ArrayList<>();

    //    String query = "Select * from Banking_Question bq inner join Chapter c on c.ChapterID = bq.ChapterID where c.ChapterID = ? ";
        String query = "Select c.* from chapter c, subject s where c.subjectid =  s.subjectid and s.SubjectID = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setInt(1, subjectID);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            int chapterID = resultSet.getInt("ChapterID");
            String chapterName = resultSet.getString("Chapter_name");
            int subjectId = resultSet.getInt("SubjectID");

            Chapter chapter = new Chapter(chapterID, chapterName,subjectId);
            chapterList.add(chapter);
        }

        return chapterList;
    }
    
    
     public Chapter getChapterByID(int chapterID) throws SQLException {
        Chapter chapter = null;

        String query = "SELECT * FROM Chapter WHERE chapterID = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setInt(1, chapterID);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            String chapterName = resultSet.getString("chapterName");
            int subjectId = resultSet.getInt("SubjectID");

            chapter = new Chapter(chapterID, chapterName,subjectId);
        }

        // Đóng kết nối và tài nguyên
        resultSet.close();
        statement.close();

        return chapter;
    }
}
