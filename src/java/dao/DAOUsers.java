/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Users;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBConnect;
import entity.Class;

/**
 *
 * @author Admin
 */
public class DAOUsers extends DBConnect {

    public List<Users> getAllUser() {
        String sql = "Select * from Account";
        List<Users> list = new ArrayList<>();
        String account, password, email, address, phone;
        int accountID, roleID, avatarID;
        try {
            Statement state = conn.createStatement();
            PreparedStatement pre = conn.prepareStatement(sql);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                accountID = rs.getInt("accountID");
                account = rs.getString("account");
                password = rs.getString("password");
                email = rs.getString("email");
                roleID = rs.getInt("roleID");
                phone = rs.getString("roleID");
                address = rs.getString("address");
                avatarID = rs.getInt("avatarID");
                list.add(new Users(accountID, account, password, email, roleID, phone, address, avatarID));
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public Users getUser(String account, String password) {
        String sql = "Select *from Account where account = ? and password = ?";
        Users nu = null;
        String email, address, phone;
        int accountID, roleID, avatarID;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre = conn.prepareStatement(sql);
            pre.setString(1, account);
            pre.setString(2, password);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                accountID = rs.getInt("AccountID");
                email = rs.getString("Email");
                roleID = rs.getInt("Role");
                phone = rs.getString("Phone");
                address = rs.getString("Address");
                avatarID = rs.getInt("AvatarID");
                nu = new Users(accountID, account, password, email, roleID, phone, address, avatarID);
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nu;
    }

    //Kiểm tra xem Email đã tồn tại
    public Users getUserByEmail(String email) {
        String sql = "Select * from Account where email = ?";
        Users nu = null;
        String account, password, address, phone;
        int accountID, roleID, avatarID;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, email.trim());
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                accountID = rs.getInt("AcountID");
                account = rs.getString("Account");
                password = rs.getString("Password");
                roleID = rs.getInt("Role");
                phone = rs.getString("Phone");
                address = rs.getString("Address");
                avatarID = rs.getInt("AvatarID");
                nu = new Users(accountID, account, password, email, roleID, phone, address, avatarID);
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nu;
    }

    //Kiểm tra xem Email đã tồn tại
    public Users getUserByEmail1(String email) {
        String sql = "Select * from Account where Email = '" + email + "' ";
        String email1 = "teacher1@fe.edu.vn";
        Users nu = null;
        String account, password, address, phone;
        int accountID, roleID, avatarID;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
//            pre.setString(1, email1);

            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                accountID = rs.getInt("AccountID");
                account = rs.getString("Account");
                password = rs.getString("Password");
                roleID = rs.getInt("Role");
                phone = rs.getString("Phone");
                address = rs.getString("Address");
                avatarID = rs.getInt("AvatarID");
                nu = new Users(accountID, account, password, email, roleID, phone, address, avatarID);

            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nu;
    }

    // Kiểm tra xem account đã tồn tại
    public Users getUser(String account) {
        String sql = "select * from account where account = ?";
        Users nu = null;
        String password, email, phone, address;
        int accountID, roleID, avatarID;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, account);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                accountID = rs.getInt("accountID");
                password = rs.getString("password");
                email = rs.getString("email");
                roleID = rs.getInt("roleID");
                phone = rs.getString("phone");
                address = rs.getString("address");
                avatarID = rs.getInt("avatarID");
                nu = new Users(accountID, account, password, email, roleID, phone, address, avatarID);;
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nu;
    }

    public void insert(String account, String password, String email, int roleID, String phone, String address, int avatarID) {
//        String sql = "INSERT INTO Users\n"
//                + "           ,[name]\n"
//                + "           ,[email])\n"
//                + "           ,[password])\n"
//                + "           ,[role])\n"
//                + "     VALUES(?,?,?,?)";
        String sql = "Insert into account (account, password, email, roleID, phone, address, avatarID) values (?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, account);
            pre.setString(2, password);
            pre.setString(3, email);
            pre.setInt(4, roleID);
            pre.setString(5, phone);
            pre.setString(6, address);
            pre.setInt(7, avatarID);
            pre.executeUpdate();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUsers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Update password
    public boolean updatePassword(int accountID, String newPassword) throws SQLException {
        String query = "UPDATE Account SET password = ? WHERE accountID = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, newPassword);
        statement.setInt(2, accountID);
        int rowsAffected = statement.executeUpdate();

        return rowsAffected > 0;
    }

    //Update password
    public Users getUserByID(int accountID) throws SQLException {
        String query = "SELECT * FROM Account WHERE accountID = ?";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setInt(1, accountID);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            Users user = new Users();
            user.setAccountID(resultSet.getInt("AccountID"));
            user.setAccount(resultSet.getString("Account"));
            user.setPassword(resultSet.getString("Password"));
            user.setEmail(resultSet.getString("Email"));
            user.setRoleID(resultSet.getInt("Role"));
            user.setPhone(resultSet.getString("Phone"));
            user.setAddress(resultSet.getString("Address"));
            user.setAvatarID(resultSet.getInt("AvatarID"));
            return user;
        }

        return null;
    }
    //forget
    public void forgetPassword(Users model) {
        String query = "UPDATE [dbo].[Account]\n"
                + "   SET [Password] = ?\n"
                + " WHERE AccountID = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(query);
            pre.setString(1, model.getPassword());
            pre.setInt(2, model.getAccountID());
            pre.executeUpdate();
        } catch (SQLException ex) {
        }
    }
    
    // Show the classroom on each separate screen for the teacher
     public List<entity.Class> getClassesByTeacherID(int teacherID) {
        List<entity.Class> classes = new ArrayList<>();
        String query = "SELECT * FROM Class WHERE teacherID = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, teacherID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                entity.Class cls = new entity.Class();
                cls.setClassID(resultSet.getString("ClassID"));
                cls.setSemester(resultSet.getString("Semester"));
                classes.add(cls);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return classes;
    }

}
