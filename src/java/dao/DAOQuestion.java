/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Test;
import entity.Answer;
import entity.Chapter;
import entity.Question;
import entity.Users;
import jakarta.servlet.jsp.jstl.sql.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBConnect;

/**
 *
 * @author Admin
 */
public class DAOQuestion extends DBConnect {

    public List<Question> getAllQuestionsByChapter(int chapterID) {
        List<Question> questions = new ArrayList<>();
        String query = "SELECT * FROM Question WHERE ChapterID = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(query);
            pre = conn.prepareStatement(query);
            pre.setInt(1, chapterID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                int questionID = rs.getInt("QuestionID");
                String content = rs.getString("Content");
                boolean typeOfQuestion = rs.getBoolean("TypeOfQuestion");

//                   List<Answer> answers = getAnswersByQuestion(questionID);
                // Question question = new Question(questionID, content, chapterID);
                questions.add(new Question(questionID, content, chapterID, typeOfQuestion));
            }
            pre.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return questions;
    }

    private List<Answer> getAnswersByQuestion(int questionID) {
        List<Answer> answers = new ArrayList<>();
        String query = "SELECT * FROM Answer WHERE QuestionID = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(query);
            pre.setInt(1, questionID);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                int answerID = rs.getInt("AnswerID");
                String answerText = rs.getString("Answer_text");
                Boolean correctAns = rs.getBoolean("is_correct");
                float answerPoint = rs.getFloat("Answer_point");

                Answer answer = new Answer(answerID, answerText, correctAns, answerPoint, questionID);
                answers.add(answer);
            }
            pre.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return answers;
    }

    public void addQuestion(Question question) throws SQLException {
        String query = "INSERT INTO Question (Content, ChapterID) VALUES (?, ?)";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setString(1, question.getContent());
            statement.setInt(2, question.getChapterID());
            statement.executeUpdate();
        }
    }

    public void insertQuestion(String Content, int ChapterID) {
        String sql = "INSERT INTO Question (Content, ChapterID) VALUES (?, ?)";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, Content);
            pre.setInt(2, ChapterID);
            pre.executeUpdate();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteQuestion(int questionID) throws SQLException {
        String query = "DELETE FROM Question WHERE QuestionID = ?";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, questionID);
            statement.executeUpdate();
        }
    }

    public void deleteAnswer(int answerID) throws SQLException {
        String query = "DELETE FROM Answer WHERE AnswerID = ?";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, answerID);
            statement.executeUpdate();
        }
    }

    public void insertAnswer(String answers, boolean isCorrect, float answerPoints, int questionID) {
        String sql = "INSERT INTO Answer (Answer_text, is_correct, Answer_point, QuestionID) VALUES (?, ?, ?, ?)";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);

            pre.setString(1, answers);
            pre.setBoolean(2, isCorrect);
            pre.setFloat(3, answerPoints);
            pre.setInt(4, questionID);
            pre.executeUpdate();

            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getQuestionID(String content, int chapterID) {
        String sql = "Select QuestionID from Question where ChapterID = ? and Content like ?";
        int questionID = 0;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, chapterID);
            pre.setString(2, content);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                questionID = rs.getInt("QuestionID");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return questionID;
    }

    public List<Question> getAllQuestion() {
        List<Question> list = new ArrayList<>();
        String sql = "select * from Question";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question q = new Question();
                q.setQuestionID(rs.getInt(1));
                q.setContent(rs.getString(2));
                q.setChapterID(rs.getInt(3));
                q.setTypeOfQuestion(rs.getBoolean(4));
                list.add(q);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Chapter> getAllChapter() {
        List<Chapter> list = new ArrayList<>();
        String sql = "select * from Chapter";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Chapter q = new Chapter();
                q.setChapterID(rs.getInt(1));
                q.setChapterName(rs.getString(2));
                q.setSubjectID(rs.getInt(3));
                list.add(q);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void insertTest(int quizID, int questionID) {
        String sql = "INSERT INTO Test(QuizID,QuestionID) VALUES ( ?, ?)";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);

            pre.setInt(1, quizID);
            pre.setInt(2, questionID);
            pre.executeUpdate();

            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Answer> getAllAnswerByQuestion(int questionID) {
        List<Answer> list = new ArrayList<>();
        String sql = "SELECT * FROM Answer WHERE QuestionID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, questionID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer an = new Answer();
                an.setAnswerID(rs.getInt(1));
                an.setAnswerText(rs.getString(2));
                an.setIs_Correct(rs.getBoolean(3));
                an.setAnswerPoint(rs.getFloat(4));
                an.setQuestionID(rs.getInt(5));
                list.add(an);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void deleteQuestion1(int questionID) {
        try {
            // Chuẩn bị câu lệnh SQL
            String sql = "DELETE FROM Question WHERE QuestionID = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, questionID);

            // Thực thi câu lệnh SQL
            statement.executeUpdate();

            // Đóng kết nối và giải phóng tài nguyên
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteTest(int questionID) {
        try {
            // Chuẩn bị câu lệnh SQL
            String sql = "DELETE FROM Test WHERE QuestionID = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, questionID);

            // Thực thi câu lệnh SQL
            statement.executeUpdate();

            // Đóng kết nối và giải phóng tài nguyên
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAnswernew(int questionID) {
        try {
            // Chuẩn bị câu lệnh SQL
            String sql = "DELETE FROM Answer WHERE QuestionID = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, questionID);

            // Thực thi câu lệnh SQL
            statement.executeUpdate();

            // Đóng kết nối và giải phóng tài nguyên
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Question> searchQuestions(String keyword,int chapterID) {
        List<Question> searchResults = new ArrayList<>();
        try {

            String sql = "SELECT * FROM Question WHERE content LIKE ? and chapterID =?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + keyword + "%");
            statement.setInt(2, chapterID);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int questionID = resultSet.getInt("QuestionID");
                String content = resultSet.getString("Content");
                int chapterrID = resultSet.getInt("ChapterID");
                boolean typeOfQuestion = resultSet.getBoolean("TypeOfQuestion");
                Question question = new Question(questionID, content, chapterrID, typeOfQuestion);
                searchResults.add(question);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Đóng resultSet, statement và connection (nếu cần)
        }
        return searchResults;
    }

    public String getQuestionContentByContent(String content) {
        String sql = "SELECT Content FROM question WHERE Content LIKE ?";
        String questionContent = null;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, content.trim());
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                questionContent = rs.getString("Content");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return questionContent;
    }

    public static void main(String[] args) {
        DAOQuestion dao = new DAOQuestion();
        int questionID = 24;
        dao.deleteAnswernew(questionID);
        dao.deleteTest(questionID);

        dao.deleteQuestion1(questionID);
    }
}

