/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.SubjectList;
import entity.ClassList;
import entity.Subject;
import entity.Users;
import entity.Class;
import entity.Student;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBConnect;

/**
 *
 * @author FPT
 */
public class DAOClass extends DBConnect {

    public List<Class> getAllClass() {
        List<Class> list = new ArrayList<>();
        String sql = "select * from Class";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Class cl = new Class();
                cl.setClassID(rs.getString(1));
                cl.setTeacherID(rs.getInt(2));
                cl.setSemester(rs.getString(3));
                list.add(cl);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Class> getClassByTeacherID(int TeacherID) {
        List<Class> list = new ArrayList<>();
        String sql = "SELECT [ClassID],[Semester],Class.[TeacherID],[SubjectID] "
                + "FROM [Class] join [Teacher] on Class.TeacherID=Teacher.TeacherID "
                + "where Class.TeacherID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, TeacherID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Class sb = new Class();
                sb.setClassID(rs.getString(1));
                sb.setSemester(rs.getString(2));
                sb.setTeacherID(rs.getInt(3));
                list.add(sb);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void createClass(String classID, String teacherID, String semester) {

        String sql = "INSERT INTO [dbo].Class\n"
                + "                ([ClassID],[TeacherID],[Semester])\n"
                + "                VALUES (?, ?, ?)";
        try {
            PreparedStatement ptm = conn.prepareStatement(sql);
            ptm.setString(1, classID);
            ptm.setString(2, teacherID);
            ptm.setString(3, semester);
            ptm.executeUpdate();
            ptm.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map<String, List<?>> getAllClassAndAccount() {
        Map<String, List<?>> resultMap = new HashMap<>();
        List<Class> classList = new ArrayList<>();
        List<Users> accountList = new ArrayList<>();
        String sql = "SELECT c.ClassID, c.TeacherID, c.Semester, a.Account "
                + "FROM Class c "
                + "INNER JOIN Teacher t ON c.TeacherID = t.TeacherID "
                + "INNER JOIN Account a ON t.AccountID = a.AccountID";
        try ( PreparedStatement ps = conn.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Class cl = new Class();
                cl.setClassID(rs.getString("ClassID"));
                cl.setTeacherID(rs.getInt("TeacherID"));
                cl.setSemester(rs.getString("Semester"));
                classList.add(cl);

                Users account = new Users();
                account.setAccount(rs.getString("Account"));
                accountList.add(account);
            }
            resultMap.put("classes", classList);
            resultMap.put("accounts", accountList);
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultMap;
    }

    public Class getClasstById(String id) {
        String sql = "SELECT * from class where classID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Class sb = new Class();
                sb.setClassID(rs.getString(1));
                sb.setTeacherID(rs.getInt(2));
                sb.setSemester(rs.getString(3));
                return sb;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Map<String, List<?>> getTeacherClassAndSubject(int TeacherID, String SubjectCode, String Semester) {
        Map<String, List<?>> resultMap = new HashMap<>();
        List<Class> classList = new ArrayList<>();
        List<Subject> subjectList = new ArrayList<>();
        String sql = "select  c.ClassID,c.TeacherID,c.Semester,s.SubjectID,s.SubjectCode from class c \n"
                + "				join SubjectList sl on sl.ClassID=c.ClassID\n"
                + "                join subject s on sl.SubjectID=s.SubjectID \n"
                + "                join Teacher t on t.TeacherID=c.TeacherID \n"
                + "                where c.TeacherID = ? and s.SubjectCode=? and c.Semester=?";
        try ( PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, TeacherID);
            ps.setString(2, SubjectCode);
            ps.setString(3, Semester);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Class cl = new Class();
                    cl.setClassID(rs.getString("ClassID"));
                    cl.setTeacherID(rs.getInt("TeacherID"));
                    cl.setSemester(rs.getString("Semester"));
                    classList.add(cl);

                    Subject sb = new Subject();
                    sb.setSubjectCode(rs.getString("SubjectCode"));
                    sb.setSubjectID(rs.getInt("SubjectID"));
                    subjectList.add(sb);
                }
            }
            resultMap.put("classes", classList);
            resultMap.put("subjects", subjectList);
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultMap;
    }

    public Map<String, List<?>> getTeacherSubjectSemester(int TeacherID) {
        Map<String, List<?>> resultMap = new HashMap<>();
        List<Class> classList = new ArrayList<>();
        List<Subject> subjectList = new ArrayList<>();
        String sql = "SELECT distinct s.SubjectCode,s.SubjectID, c.Semester FROM [Class] c \n"
                + "	join [Teacher] t on c.TeacherID=t.TeacherID\n"
                + "	join [SubjectList] cl on c.ClassID=cl.ClassID\n"
                + "    join [Subject] s on s.SubjectID=cl.SubjectID where c.TeacherID =?";
        try ( PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, TeacherID);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Class cl = new Class();
                    cl.setSemester(rs.getString("Semester"));
                    classList.add(cl);

                    Subject sb = new Subject();
                    sb.setSubjectID(rs.getInt("SubjectID"));
                    sb.setSubjectCode(rs.getString("SubjectCode"));
                    subjectList.add(sb);
                }
            }
            resultMap.put("classes", classList);
            resultMap.put("subjects", subjectList);
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultMap;
    }

    public void deleteClassByID(String id) {
        String sql = "DELETE FROM [dbo].[Class]\n"
                + "      WHERE ClassID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateClass(String classID, String teacherID, String semester) {
        String sql = "UPDATE class\n"
                + "   SET [teacherID] = ?\n"
                + "      ,[semester] = ?\n"
                + " WHERE [classID] = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);

            pre.setString(1, teacherID);
            pre.setString(2, semester);
            pre.setString(3, classID);
            // run
            pre.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void addStudentToClass(String classID, String studentID) {

        String sql = "INSERT INTO [dbo].[classlist]\n"
                + "([classID],[studentID])\n"
                + "VALUES (?,?)";
        try {
            PreparedStatement ptm = conn.prepareStatement(sql);
            ptm.setString(1, classID);
            ptm.setString(2, studentID);
            ptm.executeUpdate();
            ptm.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addSubjectToClass(String classID, String subjectID) {

        String sql = "INSERT INTO [dbo].[subjectlist]\n"
                + "([classID],[subjectID])\n"
                + "VALUES (?,?)";
        try {
            PreparedStatement ptm = conn.prepareStatement(sql);
            ptm.setString(1, classID);
            ptm.setString(2, subjectID);
            ptm.executeUpdate();
            ptm.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map<String, List<?>> getClassList(String classID) {
        Map<String, List<?>> resultMap = new HashMap<>();
        List<Class> classList = new ArrayList<>();
        List<Student> studentList = new ArrayList<>();
        String sql = "select * from ClassList c,Student s where c.ClassID like ? and c.StudentID=s.StudentID";
        try ( PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, classID);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Class cl = new Class();
                    cl.setClassID(rs.getString("classID"));
                    classList.add(cl);

                    Student st = new Student();
                    st.setStudentID(rs.getInt("StudentID"));
                    st.setName(rs.getString("Name"));
                    st.setAge(rs.getInt("Age"));
                    st.setGender(rs.getString("gender"));
                    st.setAccountID(rs.getInt("accountID"));
                    studentList.add(st);
                }
            }
            resultMap.put("classes", classList);
            resultMap.put("students", studentList);
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultMap;
    }

    public Map<String, List<?>> getSubjectList(String classID) {
        Map<String, List<?>> resultMap = new HashMap<>();
        List<Class> classList = new ArrayList<>();
        List<Subject> subjectList = new ArrayList<>();
        String sql = "select * from SubjectList sl,Subject s where sl.ClassID like ? and sl.SubjectID=s.SubjectID";
        try ( PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, classID);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Class cl = new Class();
                    cl.setClassID(rs.getString("classID"));
                    classList.add(cl);

                    Subject st = new Subject();
                    st.setSubjectID(rs.getInt("SubjectID"));
                    st.setSubjectName(rs.getString("SubjectName"));
                    st.setSubjectCode(rs.getString("SubjectCode"));
                    subjectList.add(st);
                }
            }
            resultMap.put("classes", classList);
            resultMap.put("subjects", subjectList);
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultMap;
    }

    public Student getStudentById(String id) {
        String sql = "select * from Student where studentID =  ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Student s = new Student();
                s.setStudentID(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setAge(rs.getInt(3));
                s.setGender(rs.getString(4));
                s.setAccountID(rs.getInt(5));
                return s;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ClassList getStudentClass(String id, String ClassID) {
        String sql = "select * from ClassList where studentID =  ? and classID like ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ps.setString(2, ClassID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                ClassList s = new ClassList();
                s.setStudentID(rs.getInt(1));
                s.setClassID(rs.getString(2));
                return s;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Class> getAllSemester() {
        List<Class> list = new ArrayList<>();
        String sql = "select DISTINCT semester from class";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Class q = new Class();
                q.setSemester(rs.getString(1));
                list.add(q);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void deleteCLassList(String classID, int studentID) {
        String sql = "DELETE FROM [dbo].[ClassList]\n"
                + "      WHERE ClassID like ? and StudentID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, classID);
            ps.setInt(2, studentID);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteSubjectList(String classID, int subjectID) {
        String sql = "DELETE FROM [dbo].[SubjectList]\n"
                + "      WHERE ClassID like ? and subjectID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, classID);
            ps.setInt(2, subjectID);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(DAOClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
