/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Chapter;
import entity.Subject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBConnect;

/**
 *
 * @author Admin
 */
public class DAOSubject extends DBConnect {

    public List<Subject> getAllSubject() {
        List<Subject> list = new ArrayList<>();
        String sql = "select * from Subject";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Subject su = new Subject();
                su.setSubjectID(rs.getInt(1));
                su.setSubjectName(rs.getString(2));
                su.setSubjectCode(rs.getString(3));
                list.add(su);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int updateSubject(Subject sub) {
        int n = 0;
        String sql = "UPDATE subject\n"
                + "   SET [SubjectName] = ?\n"
                + "      ,[SubjectCode] = ?\n"
                + " WHERE [SubjectID] = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);

            pre.setInt(3, sub.getSubjectID());
            pre.setString(1, sub.getSubjectName());
            pre.setString(2, sub.getSubjectCode());
            // run
            n = pre.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public Subject getSubjectById(int id) {
        String sql = "SELECT * from Subject where SubjectID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Subject sb = new Subject();
                sb.setSubjectID(rs.getInt(1));
                sb.setSubjectName(rs.getString(2));
                sb.setSubjectCode(rs.getString(3));
                return sb;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateSubject1(String SubjectID, String SubjectName, String SubjectCode) {
        String sql = "UPDATE subject\n"
                + "   SET [SubjectName] = ?\n"
                + "      ,[SubjectCode] = ?\n"
                + " WHERE [SubjectID] = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);

            pre.setString(1, SubjectName);
            pre.setString(2, SubjectCode);
            pre.setString(3, SubjectID);
            // run
            pre.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createSubject(String subjectName, String subjectCode) {

        String sql = "INSERT INTO [dbo].[subject]\n"
                + "([subjectName],[subjectCode])\n"
                + "VALUES (?,?)";
        try {
            PreparedStatement ptm = conn.prepareStatement(sql);
            ptm.setString(1, subjectName);
            ptm.setString(2, subjectCode);
            ptm.executeUpdate();
            ptm.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Subject getNewSubject() {
        String sql = "Select Top 1 SubjectID from subject order by SubjectID desc";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Subject sb = new Subject();
                sb.setSubjectID(rs.getInt(1));
                return sb;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Subject getSubjectByCode(String subjectCode) {
        String sql = "Select * from Subject where subjectCode = ?";
        Subject sub = null;
        String subjectName;
        int subjectID;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, subjectCode.trim());
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                subjectID = rs.getInt("subjectID");
                subjectName = rs.getString("subjectName");
                sub = new Subject(subjectID, subjectName, subjectCode);
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sub;
    }

    public Subject getSubjectByIdstr(String id) {
        String sql = "SELECT * from Subject where SubjectID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Subject sb = new Subject();
                sb.setSubjectID(rs.getInt(1));
                sb.setSubjectName(rs.getString(2));
                sb.setSubjectCode(rs.getString(3));
                return sb;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Subject getSubjectString(String id) {
        String sql = "SELECT * from Subject where SubjectID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Subject sb = new Subject();
                sb.setSubjectID(rs.getInt(1));
                sb.setSubjectName(rs.getString(2));
                sb.setSubjectCode(rs.getString(3));
                return sb;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
