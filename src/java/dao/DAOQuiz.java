/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Question;
import entity.Quiz;
import entity.Test;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBConnect;

/**
 *
 * @author dell
 */
public class DAOQuiz extends DBConnect {

    public List<Quiz> getAllQuizzes() throws SQLException {
        List<Quiz> quizzes = new ArrayList<>();
        String query = "SELECT * FROM Quiz";
        try ( PreparedStatement statement = conn.prepareStatement(query);  ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Quiz quiz = new Quiz();
                quiz.setQuizId(resultSet.getInt("QuizID"));
                quiz.setQuizTitle(resultSet.getString("QuizTitle"));
                quiz.setNumberQuestion(resultSet.getInt("NumberQuestion"));
                quiz.setStartDate(resultSet.getString("StartDate"));
                quiz.setEndDate(resultSet.getString("EndDate"));
                quiz.setTimes(resultSet.getString("Times"));
                quiz.setSubjectId(resultSet.getInt("SubjectID"));
                quizzes.add(quiz);
            }
        }
        return quizzes;
    }

    public Quiz getQuizById(int quizId) throws SQLException {
        String query = "SELECT * FROM Quiz WHERE QuizID = ?";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, quizId);
            try ( ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Quiz quiz = new Quiz();
                    quiz.setQuizId(resultSet.getInt("QuizID"));
                    quiz.setQuizTitle(resultSet.getString("QuizTitle"));
                    quiz.setNumberQuestion(resultSet.getInt("NumberQuestion"));
                    quiz.setStartDate(resultSet.getString("StartDate"));
                    quiz.setEndDate(resultSet.getString("EndDate"));
                    quiz.setTimes(resultSet.getString("Times"));
                    quiz.setSubjectId(resultSet.getInt("SubjectID"));
                    return quiz;
                }
            }
        }
        return null;
    }

    public Quiz getQuizByIdt(int quizId) {
        String query = "SELECT * FROM Quiz WHERE QuizID = ?";
        try (
                 PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, quizId);
            try ( ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Quiz quiz = new Quiz();
                    quiz.setQuizId(resultSet.getInt("QuizID"));
                    quiz.setQuizTitle(resultSet.getString("QuizTitle"));
                    quiz.setNumberQuestion(resultSet.getInt("NumberQuestion"));
                    quiz.setStartDate(resultSet.getString("StartDate"));
                    quiz.setEndDate(resultSet.getString("EndDate"));
                    quiz.setTimes(resultSet.getString("Times"));
                    quiz.setSubjectId(resultSet.getInt("SubjectID"));
                    return quiz;
                }
            }
        } catch (SQLException e) {
            // Handle the SQL exception here, you can log the error or throw a custom exception
            e.printStackTrace();
        }
        return null;
    }

    public void createQuiz(Quiz quiz) throws SQLException {
        String query = "INSERT INTO Quiz (QuizTitle,Description, NumberQuestion, StartDate, EndDate, Times, SubjectID,Status) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?,?)";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setString(1, quiz.getQuizTitle());
            statement.setString(2, quiz.getDescription());
            statement.setInt(3, quiz.getNumberQuestion());
            statement.setString(4, quiz.getStartDate());
            statement.setString(5, quiz.getEndDate());
            statement.setString(6, quiz.getTimes());
            statement.setInt(7, quiz.getSubjectId());
            statement.setInt(8, 1);
            statement.executeUpdate();
        }
    }

    public void updateQuiz(Quiz quiz) throws SQLException {
        String query = "UPDATE Quiz SET QuizTitle = ?, NumberQuestion = ?, StartDate = ?, EndDate = ?, "
                + "Times = ?, SubjectID = ? WHERE QuizID = ?";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setString(1, quiz.getQuizTitle());
            statement.setInt(2, quiz.getNumberQuestion());
            statement.setString(3, quiz.getStartDate());
            statement.setString(4, quiz.getEndDate());
            statement.setString(5, quiz.getTimes());
            statement.setInt(6, quiz.getSubjectId());
            statement.setInt(7, quiz.getQuizId());
            statement.executeUpdate();
        }
    }

    public void deleteQuiz(int quizId) {
        String query = "DELETE FROM Quiz WHERE QuizID = ?";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, quizId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteTest(int quizId) {
        String query = "DELETE FROM Test WHERE QuizID = ?";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, quizId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getQuizID(String QuizTitle, String Description, int NumberQuestion, String StartDate, String EndDate, String Times, int SubjectID) {
        String sql = "select top 1 QuizID from Quiz where QuizTitle like ? and Description like ? and NumberQuestion=? and StartDate =? and EndDate =? and SubjectID=? and Times=? order by QuizID desc";
        int quizID = 0;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, QuizTitle);
            pre.setString(2, Description);
            pre.setInt(3, NumberQuestion);
            pre.setString(4, StartDate);
            pre.setString(5, EndDate);
            pre.setInt(6, SubjectID);
            pre.setString(7, Times);

            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                quizID = rs.getInt("QuizID");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
        return quizID;
    }

    public List<Question> getQuizAndQuestion(int quizID) {
        List<Question> list = new ArrayList<>();
        String sql = "select Question.QuestionID,Question.Content,Question.ChapterID from quiz, Test,Question "
                + "where quiz.QuizID=Test.QuizID and test.QuestionID=Question. QuestionID and Quiz.QuizID=?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, quizID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question an = new Question();
                an.setQuestionID(rs.getInt(1));
                an.setContent(rs.getString(2));
                an.setChapterID(rs.getInt(3));
                list.add(an);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getTopQuiz() {
        String sql = "SELECT TOP 1 [QuizID]\n"
                + "  FROM [Quiz] order by QuizID ";
        int topQuizID = 0;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                topQuizID = rs.getInt("QuizID");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return topQuizID;
    }

    public List<Quiz> getQuizBySubjectID(int subjectID) {
        List<Quiz> list = new ArrayList<>();
        String sql = "select QuizID,QuizTitle,StartDate,EndDate,Status,subjectID from Quiz where subjectID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, subjectID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Quiz q = new Quiz();
                q.setQuizId(rs.getInt(1));
                q.setQuizTitle(rs.getString(2));
                q.setStartDate(rs.getString(3));
                q.setEndDate(rs.getString(4));
                q.setStatus(rs.getInt(5));
                q.setSubjectId(rs.getInt(6));
                list.add(q);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateStatus(int quizID, int status) {
        String sql = "UPDATE quiz\n"
                + "                  SET [Status] = ? \n"
                + "                WHERE [quizID] = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);

            pre.setInt(1, status);
            pre.setInt(2, quizID);
            // run
            pre.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Test> getQuizTest(int quizID) {
        List<Test> list = new ArrayList<>();
        String sql = "select distinct TestID,QuizID from Test where QuizID=?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, quizID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Test an = new Test();
                an.setTestID(rs.getInt(1));
                an.setQuizID(rs.getInt(2));
                list.add(an);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void deleteQuesInTest(int quizId, int questionId) {
        String query = "DELETE FROM Test WHERE QuizID = ? and QuestionID = ?";
        try ( PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, quizId);
            statement.setInt(2, questionId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        DAOQuiz dao = new DAOQuiz();
        List<Test> list = dao.getQuizTest(2);
        for (Test test : list) {
            System.out.println("Test ID: " + test.getTestID());
            System.out.println("Quiz ID: " + test.getQuizID());
            System.out.println("--------------------------");
        }
    }
}
