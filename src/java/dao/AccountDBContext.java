/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Student;
import entity.Teacher;
import entity.Users;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBConnect;

/**
 *
 * @author Admin
 */
public class AccountDBContext extends DBConnect {

    public void createAccount(String account, String email, String address, String phone, String password, String role, String fullname, String age, String gender) {
        String sql = "INSERT INTO [dbo].[Account]\n"
                + "           ([Account]\n"
                + "           ,[Password]\n"
                + "           ,[Email]\n"
                + "           ,[Role]\n"
                + "           ,[Phone]\n"
                + "           ,[Address])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?)";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);
            ps.setString(3, email);
            ps.setString(4, role);
            ps.setString(5, phone);
            ps.setString(6, address);
            ps.executeUpdate();
            String xSQL;
            if (role.equals("2")) {
                xSQL = "INSERT INTO [dbo].[Student]\n"
                        + "           ([Name]\n"
                        + "           ,[Age]\n"
                        + "           ,[Gender]\n"
                        + "           ,[AccountID])\n"
                        + "     VALUES\n"
                        + "           (?,?,?,?)";
            } else {
                xSQL = "INSERT INTO [dbo].[Teacher]\n"
                        + "           ([Name]\n"
                        + "           ,[Age]\n"
                        + "           ,[Gender]\n"
                        + "           ,[AccountID])\n"
                        + "     VALUES\n"
                        + "           (?,?,?,?)";
            }
            Users a = getNewAccount();
            PreparedStatement ptm = conn.prepareStatement(xSQL);
            ptm.setString(1, fullname);
            ptm.setInt(2, Integer.parseInt(age));
            ptm.setBoolean(3, gender.equals("female"));
            ptm.setInt(4, a.getAccountID());
            ptm.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Users getNewAccount() {
        String sql = "Select Top 1 AccountID from Account order by AccountID desc";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Users a = new Users();
                a.setAccountID(rs.getInt(1));
                return a;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Users> getAllUsers() {
        List<Users> list = new ArrayList<>();
        String sql = "select * from Account where [Role] != 1";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Users u = new Users();
                u.setAccountID(rs.getInt(1));
                u.setAccount(rs.getString(2));
                u.setPassword(rs.getString(3));
                u.setEmail(rs.getString(4));
                u.setRoleID(rs.getInt(5));
                u.setPhone(rs.getString(6));
                u.setAddress(rs.getString(7));
                u.setAvatarID(rs.getInt(8));
                list.add(u);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Users getUsersById(int id) {
        String sql = "SELECT * from Account where AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Users u = new Users();
                u.setAccountID(rs.getInt(1));
                u.setAccount(rs.getString(2));
                u.setPassword(rs.getString(3));
                u.setEmail(rs.getString(4));
                u.setRoleID(rs.getInt(5));
                u.setPhone(rs.getString(6));
                u.setAddress(rs.getString(7));
                u.setAvatarID(rs.getInt(8));
                return u;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Student getStudentById(int id) {
        String sql = "SELECT *\n"
                + "  FROM [QUIZ1].[dbo].[Student] where AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Student s = new Student();
                s.setAccountID(id);
                s.setStudentID(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setAge(rs.getInt(3));
                s.setGender(rs.getString(4));
                return s;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Teacher getTeacherById(int id) {
        String sql = "Select * from Teacher where AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Teacher s = new Teacher();
                s.setAccountID(id);
                s.setTeacherID(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setAge(rs.getInt(3));
                s.setGender(rs.getString(4));
                return s;
            }
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void deleteAccountById(int id) {
        String sql = "DELETE FROM [dbo].[Account]\n"
                + "      WHERE AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteStudentById(int id) {
        String sql = "DELETE FROM [dbo].[Student]\n"
                + "      WHERE AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteTeacherById(int id) {
        String sql = "DELETE FROM [dbo].[Teacher]\n"
                + "      WHERE AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void update(String id, String account, String email, String phone, String address, String name, String age, String gender) {
        String sql = "UPDATE [dbo].[Account]\n"
                + "   SET [Account] =?\n"
                + "      ,[Email] = ?\n"
                + "      ,[Phone] = ?\n"
                + "      ,[Address] = ?\n"
                + " WHERE AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, email);
            ps.setString(3, phone);
            ps.setString(4, address);
            ps.setInt(5, Integer.parseInt(id));
            ps.executeUpdate();
            String xSQL;
            Users a = getUsersById(Integer.parseInt(id));
            if (a.getRoleID() == 2) {
                xSQL = "UPDATE [dbo].[Student]\n"
                        + "   SET [Name] = ?\n"
                        + "      ,[Age] = ?\n"
                        + "      ,[Gender] = ?\n"
                        + " WHERE AccountID = ?";
            } else {
                xSQL = "UPDATE [dbo].[Teacher]\n"
                        + "   SET [Name] = ?\n"
                        + "      ,[Age] = ?\n"
                        + "      ,[Gender] = ?\n"
                        + " WHERE AccountID = ?";
            }
            PreparedStatement ptm = conn.prepareStatement(xSQL);
            ptm.setString(1, name);
            ptm.setInt(2, Integer.parseInt(age));
            ptm.setBoolean(3, gender.equals("2"));
            ptm.setInt(4, a.getAccountID());
            ptm.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Kiểm tra xem Email đã tồn tại
    public Users getUserBEmail(String email) {
        String sql = "Select * from Account where Email = ?";
        Users nu = null;
        String account, password, address, phone;
        int accountID, role, avatarID;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, email.trim());
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                accountID = rs.getInt("AccountID");
                account = rs.getString("Account");
                password = rs.getString("password");
                role = rs.getInt("role");
                phone = rs.getString("phone");
                address = rs.getString("address");
                avatarID = rs.getInt("avatarID");
                nu = new Users(accountID, account, password, email, role, phone, address, avatarID);
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nu;
    }

    //Kiểm tra xem Acc đã tồn tại
    public Users getAccByAcc(String account) {
        String sql = "Select * from Account where Account = ?";
        Users nu = null;
        String email, password, address, phone;
        int accountID, roleID, avatarID;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, account.trim());
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                accountID = rs.getInt("accountID");
                email = rs.getString("email");
                password = rs.getString("password");
                roleID = rs.getInt("role");
                phone = rs.getString("phone");
                address = rs.getString("address");
                avatarID = rs.getInt("avatarID");
                nu = new Users(accountID, account, password, email, roleID, phone, address, avatarID);
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return nu;
    }

    public int getIDByAcc(String Account) {
        String sql = "Select accountID from Account where Account = ?";
        int accountID = 0;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, Account);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                accountID = rs.getInt("accountID");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accountID;
    }

    public int getTeacherIDByAccID(int AccountID) {
        String sql = "Select TeacherID from Teacher where AccountID = ?";
        int teacherID = 0;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, AccountID);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                teacherID = rs.getInt("teacherID");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return teacherID;
    }

    public int getStudentIDByAccID(String AccountID) {
        String sql = "Select StudentID from Student where AccountID = ?";
        int id = 0;
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, AccountID);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                id = rs.getInt("StudentID");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

}
