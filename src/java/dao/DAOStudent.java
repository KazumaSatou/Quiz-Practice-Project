/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Class;
import entity.Answer;
import entity.Question;
import entity.Quiz;
import entity.Score;
import entity.Student;
import entity.Subject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DBConnect;

/**
 *
 * @author Admin
 */
public class DAOStudent extends DBConnect {

//    public static void main(String[] args) {
//        DAOStudent dao = new DAOStudent();
//        List<Subject> stu = dao.getSubjectByStudentID(3);
//        for (Subject subject : stu) {
//            System.out.println(subject);
//        }
//
////        Student stu = dao.getStudentByAccountId(3);
////        System.out.println(stu);
////        List<Quiz> stu = dao.getAllQuizBySubjectID(1);
////        for (Quiz subject : stu) {
////            System.out.println(subject);
////        }
////           Answer stu = dao.getTrueAnswerByQuestionId(2);
////           System.out.println(stu);
//    }
    public List<Subject> getSubjectByStudentID(int StudentID) {
        List<Subject> list = new ArrayList<>();
        String sql = "SELECT Subject.SubjectID, Subject.SubjectName, Subject.SubjectCode \n"
                + "                FROM (ClassList INNER JOIN SubjectList ON ClassList.ClassID = SubjectList.ClassID) \n"
                + "                INNER JOIN Subject ON SubjectList.SubjectID = Subject.SubjectID \n"
                + "                INNER JOIN Student ON Student.StudentID = ClassList.StudentID \n"
                + "                Inner join Class on Class.ClassID=ClassList.ClassID\n"
                + "                WHERE Student.AccountID = ?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, StudentID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Subject su = new Subject();
                su.setSubjectID(rs.getInt(1));
                su.setSubjectName(rs.getString(2));
                su.setSubjectCode(rs.getString(3));
                list.add(su);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOStudent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Class> getClassByStudentID(int StudentID) {
        List<Class> list = new ArrayList<>();
        String sql = "SELECT Class.ClassID \n"
                + "                FROM (ClassList INNER JOIN SubjectList ON ClassList.ClassID = SubjectList.ClassID) \n"
                + "                INNER JOIN Subject ON SubjectList.SubjectID = Subject.SubjectID \n"
                + "                INNER JOIN Student ON Student.StudentID = ClassList.StudentID \n"
                + "                INNER join Class on Class.ClassID=ClassList.ClassID\n"
                + "                WHERE Student.AccountID =?";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, StudentID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Class cl = new Class();
                cl.setClassID(rs.getString(1));
                list.add(cl);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOStudent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Student getStudentByAccountId(int id) {
        String sql = "SELECT * FROM Student WHERE AccountID = ?;";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Student stu = new Student();
                stu.setStudentID(rs.getInt(1));
                stu.setName(rs.getString(2));
                stu.setAge(rs.getInt(3));
                stu.setGender(rs.getString(4));
                stu.setAccountID(rs.getInt(5));
                return stu;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Quiz> getAllQuizBySubjectID(int SubjectID) {
        List<Quiz> list = new ArrayList<>();
        String sql = "SELECT * FROM Quiz WHERE SubjectID = ? and status =1";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, SubjectID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Quiz quiz = new Quiz();
                quiz.setQuizId(rs.getInt(1));
                quiz.setQuizTitle(rs.getString(2));
                quiz.setDescription(rs.getString(3));
                quiz.setNumberQuestion(rs.getInt(4));
                quiz.setStartDate(rs.getString(5));
                quiz.setEndDate(rs.getString(6));
                quiz.setSubjectId(rs.getInt(7));
                quiz.setTimes(rs.getString(8));

                list.add(quiz);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    List<Question> ListQuestion = new ArrayList<>();

    public List<Question> getRandomQuestion() {

        String sql = "SELECT Top 5 * FROM Question";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
//            ps.setInt(1, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question question = new Question();
                question.setQuestionID(rs.getInt(1));
                question.setContent(rs.getString(2));
                question.setChapterID(rs.getInt(3));
                this.ListQuestion.add(question);
            }

            return ListQuestion;
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Question> getQuestionByQuizID(int quizID) {

        String sql = "SELECT qe.QuestionID,qe.Content,qe.ChapterID "
                + "FROM Test t,Quiz q,Question qe where t.QuizID=q.QuizID "
                + "and q.QuizID=? "
                + "and t.QuestionID=qe.QuestionID";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, quizID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question question = new Question();
                question.setQuestionID(rs.getInt(1));
                question.setContent(rs.getString(2));
                question.setChapterID(rs.getInt(3));
                this.ListQuestion.add(question);
            }

            return ListQuestion;
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void createTestDetail(int QuestionID, int QuizID) {

        String sql = "INSERT INTO Test_Detail(QuestionID, QuizID) VALUES (?, ?);";
        try {
            PreparedStatement ptm = conn.prepareStatement(sql);
            ptm.setInt(1, QuestionID);
            ptm.setInt(2, QuizID);
            ptm.executeUpdate();
            ptm.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    List<Answer> ListAnswer = new ArrayList<>();

    public List<Answer> getAllAnswer() {

        String sql = "SELECT * FROM Answer";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
//            ps.setInt(1, QuestionID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer ans = new Answer();
                ans.setAnswerID(rs.getInt(1));
                ans.setAnswerText(rs.getString(2));
                ans.setIs_Correct(rs.getBoolean(3));
                ans.setAnswerPoint(rs.getFloat(4));
                ans.setQuestionID(rs.getInt(5));
                ListAnswer.add(ans);
            }
            return ListAnswer;
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Answer getTrueAnswerByQuestionId(int id) {

        String sql = "select * from Answer where QuestionID = ? and is_correct = '1'";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer ans = new Answer();
                ans.setAnswerID(rs.getInt(1));
                ans.setAnswerText(rs.getString(2));
                ans.setIs_Correct(rs.getBoolean(3));
                ans.setAnswerPoint(rs.getFloat(4));
                ans.setQuestionID(rs.getInt(5));
                return ans;
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Answer> getMultiAnswersByQuestionId(int id) {
        List<Answer> correctAnswers = new ArrayList<>();
        String sql = "SELECT * FROM Answer WHERE QuestionID = ? AND is_correct = '1'";
        PreparedStatement ps;

        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Answer ans = new Answer();
                ans.setAnswerID(rs.getInt(1));
                ans.setAnswerText(rs.getString(2));
                ans.setIs_Correct(rs.getBoolean(3));
                ans.setAnswerPoint(rs.getFloat(4));
                ans.setQuestionID(rs.getInt(5));
                correctAnswers.add(ans);
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }

        return correctAnswers;
    }
    public List<Answer> getFalseAnswersByQuestionId(int id) {
        List<Answer> correctAnswers = new ArrayList<>();
        String sql = "SELECT * FROM Answer WHERE QuestionID = ? AND is_correct = '0'";
        PreparedStatement ps;

        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Answer ans = new Answer();
                ans.setAnswerID(rs.getInt(1));
                ans.setAnswerText(rs.getString(2));
                ans.setIs_Correct(rs.getBoolean(3));
                ans.setAnswerPoint(rs.getFloat(4));
                ans.setQuestionID(rs.getInt(5));
                correctAnswers.add(ans);
            }
        } catch (Exception ex) {
            Logger.getLogger(DAOSubject.class.getName()).log(Level.SEVERE, null, ex);
        }

        return correctAnswers;
    }

    public String getTimeByQuizID(int quizID) {
        String time = "";
        String sql = "Select Times from Quiz where quizID = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, quizID);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                time = rs.getString("Times");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOStudent.class.getName()).log(Level.SEVERE, null, ex);
        }

        return time.substring(0, 8);
    }

    public int getSubjectByQuizID(int quizID) {
        int subjectID = 0;
        String sql = "Select subjectID from Quiz where quizID = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, quizID);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                subjectID = rs.getInt("subjectID");
            }
            rs.close();
            pre.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOStudent.class.getName()).log(Level.SEVERE, null, ex);
        }

        return subjectID;
    }

    public void addPoint(float totalScore, int sID, int qID) {

        String sql = "INSERT INTO [dbo].[score]\n"
                + "([totalScore],[StudentID],[QuizID])\n"
                + "VALUES (?,?,?)";
        try {
            PreparedStatement ptm = conn.prepareStatement(sql);
            ptm.setFloat(1, totalScore);
            ptm.setInt(2, sID);
            ptm.setInt(3, qID);
            ptm.executeUpdate();
            ptm.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOStudent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map<String, List<?>> getStudentScore(int studentID) {
        Map<String, List<?>> resultMap = new HashMap<>();
        List<Score> scoreList = new ArrayList<>();
        List<Quiz> quizList = new ArrayList<>();
        List<Subject> subjectList = new ArrayList<>();
        String sql = "SELECT TotalScore, QuizID, QuizTitle, SubjectCode\n"
                + "FROM (\n"
                + "  SELECT s.TotalScore, q.QuizID, q.QuizTitle, sb.SubjectCode,\n"
                + "    ROW_NUMBER() OVER (PARTITION BY q.QuizID ORDER BY s.ScoreID DESC) AS rn\n"
                + "  FROM Score s\n"
                + "  JOIN Student st ON s.StudentID = st.StudentID\n"
                + "  JOIN Quiz q ON q.QuizID = s.QuizID\n"
                + "  JOIN Subject sb ON sb.SubjectID = q.SubjectID\n"
                + "  WHERE q.Status = 1 AND st.StudentID = ?\n"
                + ") AS sub\n"
                + "WHERE rn = 1;";
        try ( PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, studentID);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Score cl = new Score();
                    cl.setTotalScore(rs.getFloat("TotalScore"));
                    scoreList.add(cl);

                    Quiz sb = new Quiz();
                    sb.setQuizId(rs.getInt("QuizID"));
                    sb.setQuizTitle(rs.getString("QuizTitle"));
                    quizList.add(sb);

                    Subject ss = new Subject();
                    ss.setSubjectCode(rs.getString("SubjectCode"));
                    subjectList.add(ss);
                }
            }
            resultMap.put("scores", scoreList);
            resultMap.put("quizzes", quizList);
            resultMap.put("subjects", subjectList);
        } catch (Exception ex) {
            Logger.getLogger(DAOStudent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultMap;
    }

}
