/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author FPT
 */
public class Question {
    int questionID;
    String content;
    int chapterID;
    boolean typeOfQuestion;

    public Question() {
    }

    public Question(int questionID, String content, int chapterID, boolean typeOfQuestion) {
        this.questionID = questionID;
        this.content = content;
        this.chapterID = chapterID;
        this.typeOfQuestion = typeOfQuestion;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getChapterID() {
        return chapterID;
    }

    public void setChapterID(int chapterID) {
        this.chapterID = chapterID;
    }

    public boolean isTypeOfQuestion() {
        return typeOfQuestion;
    }

    public void setTypeOfQuestion(boolean typeOfQuestion) {
        this.typeOfQuestion = typeOfQuestion;
    }

    
    
}
