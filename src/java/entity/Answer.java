/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author FPT
 */
public class Answer {

    int answerID;
    String answerText;
    Boolean is_Correct;
    float answerPoint;
    int questionID;

    public Answer() {
    }

    public Answer(int answerID, String answerText, Boolean is_Correct, float answerPoint, int questionID) {
        this.answerID = answerID;
        this.answerText = answerText;
        this.is_Correct = is_Correct;
        this.answerPoint = answerPoint;
        this.questionID = questionID;
    }

    public int getAnswerID() {
        return answerID;
    }

    public void setAnswerID(int answerID) {
        this.answerID = answerID;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Boolean getIs_Correct() {
        return is_Correct;
    }

    public void setIs_Correct(Boolean is_Correct) {
        this.is_Correct = is_Correct;
    }

    public float getAnswerPoint() {
        return answerPoint;
    }

    public void setAnswerPoint(float answerPoint) {
        this.answerPoint = answerPoint;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    

}
