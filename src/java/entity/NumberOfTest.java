/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author FPT
 */
public class NumberOfTest {
    int NumberOfTestID;
    int testID;
    int quizID;
    int scoreID;

    public NumberOfTest() {
    }

    public NumberOfTest(int NumberOfTestID, int testID, int quizID, int scoreID) {
        this.NumberOfTestID = NumberOfTestID;
        this.testID = testID;
        this.quizID = quizID;
        this.scoreID = scoreID;
    }

    public int getNumberOfTestID() {
        return NumberOfTestID;
    }

    public void setNumberOfTestID(int NumberOfTestID) {
        this.NumberOfTestID = NumberOfTestID;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getQuizID() {
        return quizID;
    }

    public void setQuizID(int quizID) {
        this.quizID = quizID;
    }

    public int getScoreID() {
        return scoreID;
    }

    public void setScoreID(int scoreID) {
        this.scoreID = scoreID;
    }
    
    
}
