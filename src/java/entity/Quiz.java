/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.Date;

/**
 *
 * @author FPT
 */
public class Quiz {

    int QuizId;
    String QuizTitle;
    String Description;
    int NumberQuestion;
    String StartDate;
    String EndDate;
    int SubjectId;
    String Times;
    int Status;

    public Quiz() {
    }

    public Quiz(int QuizId, String QuizTitle, String Description, int NumberQuestion, String StartDate, String EndDate, int SubjectId, String Times, int Status) {
        this.QuizId = QuizId;
        this.QuizTitle = QuizTitle;
        this.Description = Description;
        this.NumberQuestion = NumberQuestion;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.SubjectId = SubjectId;
        this.Times = Times;
    }

    public int getQuizId() {
        return QuizId;
    }

    public void setQuizId(int QuizId) {
        this.QuizId = QuizId;
    }

    public String getQuizTitle() {
        return QuizTitle;
    }

    public void setQuizTitle(String QuizTitle) {
        this.QuizTitle = QuizTitle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getNumberQuestion() {
        return NumberQuestion;
    }

    public void setNumberQuestion(int NumberQuestion) {
        this.NumberQuestion = NumberQuestion;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    public int getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(int SubjectId) {
        this.SubjectId = SubjectId;
    }

    public String getTimes() {
        return Times;
    }

    public void setTimes(String Times) {
        this.Times = Times;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

}
